Projekt cím: MyCinema

- Mozik listázása, módosítása (van-e 3D, van-e felirat), felvétele, törlése
- Büfé menü listázása (étel, ital, ár, allergének stb.)
- Műsor listázása, módosítása (nézőszám, bevétel), felvétele, törlése
- Film listázása, módosítása (főszereplő, évjárat stb.), felvétele, törlése
- Aktuálisan vetített filmek listázása dátum szerint (cím, időpont, melyik moziban -> kapcsolótábla)

