﻿using AutoMapper;
using MyCinema.Data;
using MyCinema.Logic;
using MyCinema.Repository;
using MyCinema.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyCinema.Web.Controllers
{
    public class FilmApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        IFilmLogic logic;
        IMapper mapper;
        public FilmApiController()
        {
            MyCinemaDatabaseEntities ctx = new MyCinemaDatabaseEntities();
            FilmRepository filmRepo = new FilmRepository();
            logic = new FilmLogic(filmRepo);
            mapper = MapperFactory.CreateMapper();

        }

        [ActionName("all")]
        [HttpGet]
        public IEnumerable<Models.Film> GetAll()
        {
            var films = logic.GetFilms();
            return mapper.Map<IList<Data.Film>, List<Models.Film>>(films);
        }

        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneFilm(int id)
        {
            return new ApiResult() { OperationResult = logic.DeleteFilm(id) };
        }


        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneFilm(Models.Film film)
        {
            logic.CreateFilm(new Data.Film() { filmID = film.Id, cim = film.Cim, mufaj = film.Mufaj, hossz = film.Hossz, ev = film.Ev, foszereplo = film.Foszereplo });
            return new ApiResult() { OperationResult = true };
        }

        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneFilm(Models.Film film)
        {
            return new ApiResult()
            {
                OperationResult = logic.ChangeFilm(film.Id, film.Cim, film.Mufaj, film.Hossz, film.Ev, film.Foszereplo)
            };
        }
    }
}
