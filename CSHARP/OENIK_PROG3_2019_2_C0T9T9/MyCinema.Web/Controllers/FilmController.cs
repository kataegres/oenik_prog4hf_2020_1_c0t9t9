﻿using AutoMapper;
using MyCinema.Data;
using MyCinema.Logic;
using MyCinema.Repository;
using MyCinema.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyCinema.Web.Controllers
{
    public class FilmController : Controller
    {
        IFilmLogic logic;
        IMapper mapper;
        FilmViewModel model;

        public FilmController()
        {
            MyCinemaDatabaseEntities ctx = new MyCinemaDatabaseEntities();
            FilmRepository filmRepo = new FilmRepository();
            logic = new FilmLogic(filmRepo);
            mapper = MapperFactory.CreateMapper();
            model = new FilmViewModel();
            model.EditedFilm = new Models.Film();

            var films = logic.GetFilms();
            model.ListOfFilms = mapper.Map<IList<Data.Film>, List<Models.Film>>(films);
        }

        private Models.Film GetFilmModel(int id)
        {
            Data.Film oneFilm = logic.GetFilmByID(id);
            return mapper.Map<Data.Film, Models.Film>(oneFilm);
        }

        // GET: Film
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("FilmIndex", model);
        }

        public ActionResult Details(int id)
        {
            return View("FilmDetails", GetFilmModel(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete FAIL";
            if (logic.DeleteFilm(id)) TempData["editResult"] = "Delete OK";
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            model.EditedFilm = GetFilmModel(id);
            return View("FilmIndex", model);
        }

        [HttpPost]
        public ActionResult Edit(Models.Film film, string editAction)
        {
            if (ModelState.IsValid && film != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    logic.CreateFilm(new Data.Film() { cim = film.Cim, mufaj = film.Mufaj, hossz = film.Hossz, ev = film.Ev, foszereplo = film.Foszereplo });
                }
                else
                {
                    if (!logic.ChangeFilm(film.Id, film.Cim, film.Mufaj, film.Hossz, film.Ev, film.Foszereplo))
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["editAction"] = "Edit";
                model.EditedFilm = film;
                return View("CarsIndex", model);
            }
        }

    }
}
