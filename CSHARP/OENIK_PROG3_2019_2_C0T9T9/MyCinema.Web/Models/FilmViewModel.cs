﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyCinema.Web.Models
{
    public class FilmViewModel
    {
        public Film EditedFilm { get; set; }
        public List<Film> ListOfFilms { get; set; }
    }
}