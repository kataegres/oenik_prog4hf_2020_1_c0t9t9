﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyCinema.Web.Models
{

    public class Film
    {
        [Display(Name = "Film id")]
        [Required]
        public int Id { get; set; }

        [Display(Name = "Film title")]
        [Required]
        public string Cim { get; set; }

        [Display(Name = "Film genre")]
        [Required]
        public string Mufaj { get; set; }

        [Display(Name = "Film length")]
        [Required]
        public int Hossz { get; set; }

        [Display(Name = "Film release year")]
        [Required]
        public int Ev { get; set; }

        [Display(Name = "Main actor")]
        [Required]
        public string Foszereplo { get; set; }
    }
}