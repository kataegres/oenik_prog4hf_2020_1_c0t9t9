﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyCinema.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MyCinema.Data.Film, MyCinema.Web.Models.Film>().
                 ForMember(dest => dest.Id, map => map.MapFrom(src => src.filmID)).
                 ForMember(dest => dest.Cim, map => map.MapFrom(src => src.cim)).
                 ForMember(dest => dest.Mufaj, map => map.MapFrom(src => src.mufaj)).
                 ForMember(dest => dest.Hossz, map => map.MapFrom(src => src.hossz)).
                 ForMember(dest => dest.Ev, map => map.MapFrom(src => src.ev)).
                 ForMember(dest => dest.Foszereplo, map => map.MapFrom(src => src.foszereplo));
            });
            return config.CreateMapper();
        }
    }
}