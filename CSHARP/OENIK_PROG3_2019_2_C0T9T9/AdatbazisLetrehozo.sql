﻿CREATE TABLE [dbo].[Mozi] (
    [moziID]      INT          NOT NULL,
    [nev]         VARCHAR (50) NULL,
    [varos]       VARCHAR (50) NOT NULL,
    [ferohely]    NUMERIC (4)  NOT NULL,
    [vane3D]      VARCHAR (20) NOT NULL,
    [vanefelirat] VARCHAR (20) NOT NULL,
    CONSTRAINT [pk_Mozi] PRIMARY KEY CLUSTERED ([moziID] ASC)
);

CREATE TABLE [dbo].[Mozimusor] (
    [mozimusorID]   INT         NOT NULL,
    [vetites_datum] DATE        NOT NULL,
    [nezoszam]      NUMERIC (3) NULL,
    [bevetel]       NUMERIC (7) NULL,
    CONSTRAINT [PK_Mozimusor] PRIMARY KEY CLUSTERED ([mozimusorID] ASC),
    CONSTRAINT [CK_Mozimusor_bevetel] CHECK ([bevetel]>=(0))
);

CREATE TABLE [dbo].[Film] (
    [filmID]     INT          NOT NULL,
    [cim]        VARCHAR (50) NOT NULL,
    [mufaj]      VARCHAR (20) NULL,
    [hossz]      NUMERIC (7)  NULL,
    [ev]         NUMERIC (4)  NULL,
    [foszereplo] VARCHAR (50) NULL,
    CONSTRAINT [PK_Film] PRIMARY KEY CLUSTERED ([filmID] ASC)
);

CREATE TABLE [dbo].[Kapcsolo] (
    [mozimusorID] INT NOT NULL,
    [moziID]      INT NOT NULL,
    [filmID]      INT NOT NULL,
    CONSTRAINT [fk_musor] FOREIGN KEY ([mozimusorID]) REFERENCES [dbo].[Mozimusor] ([mozimusorID]) ON DELETE CASCADE,
    CONSTRAINT [fk_mozi] FOREIGN KEY ([moziID]) REFERENCES [dbo].[Mozi] ([moziID]) ON DELETE CASCADE,
    CONSTRAINT [fk_film] FOREIGN KEY ([filmID]) REFERENCES [dbo].[Film] ([filmID]) ON DELETE CASCADE
);


CREATE TABLE [dbo].[Bufe] (
    [termekID]    INT          NOT NULL,
    [nev]         VARCHAR (50) NOT NULL,
    [tipus]       VARCHAR (50) NOT NULL,
    [ar]          NUMERIC (4)  NOT NULL,
    [allergen]    NUMERIC (3)  NULL,
    [homerseklet] VARCHAR(50)  NULL,
    CONSTRAINT [pk_Bufe] PRIMARY KEY CLUSTERED ([termekID] ASC)
);

insert into Film(filmID,cim,mufaj,ev,hossz,foszereplo) values (1,'Joker','thriller',2019,122,'Joaquin Phoenix');
insert into Film(filmID,cim,mufaj,ev,hossz,foszereplo) values (2,'Pókember - Idegenben','akció',2019,97,'Tom Holland');
insert into Film(filmID,cim,mufaj,ev,hossz,foszereplo) values (3,'Eredet','akció',2010,130,'Leonardo DiCaprio');
insert into Film(filmID,cim,mufaj,ev,hossz,foszereplo) values (4,'Csillag születik','dráma',2018,123,'Lady Gaga');
insert into Film(filmID,cim,mufaj,ev,hossz,foszereplo) values (5,'Expedíció','thriller',2018,99,'Natalie Portman');
insert into Film(filmID,cim,mufaj,ev,hossz,foszereplo) values (6,'Bosszúállók - Végjáték','akció',2019,140,'Robert Downey Jr.');
insert into Film(filmID,cim,mufaj,ev,hossz,foszereplo) values (7,'A nő','dráma',2013,88,'Joaquin Phoenix');
insert into Film(filmID,cim,mufaj,ev,hossz,foszereplo) values (8,'John Wick 3','akció',2019,112,'Keanu Reeves');
insert into Film(filmID,cim,mufaj,ev,hossz,foszereplo) values (9,'Volt egyszer egy Hollywood','akció',2019,132,'Brad Pitt');
insert into Film(filmID,cim,mufaj,ev,hossz,foszereplo) values (10,'Az - Második fejezet','horror',2019,104,'James McAvoy');
insert into Film(filmID,cim,mufaj,ev,hossz,foszereplo) values (11,'Bohém Rapszódia','dráma',2018,122,'Rami Malek');
insert into Film(filmID,cim,mufaj,ev,hossz,foszereplo) values (12,'Éjszakai ragadozók','dráma',2016,105,'Jake Gyllenhaal');

insert into Mozi(moziID, nev, varos, ferohely, vane3D, vanefelirat) values (1,'Tallin Mozi','Szombathely', 270, 'igen', 'nem')
insert into Mozi(moziID, nev, varos, ferohely, vane3D, vanefelirat) values (2,'MiMozink','Budapest', 140, 'nem', 'nem')
insert into Mozi(moziID, nev, varos, ferohely, vane3D, vanefelirat) values (3,'Apolló Mozi','Pécs', 210, 'igen', 'igen')
insert into Mozi(moziID, nev, varos, ferohely, vane3D, vanefelirat) values (4,'BékésCinema','Békéscsaba', 120, 'igen', 'nem')
insert into Mozi(moziID, nev, varos, ferohely, vane3D, vanefelirat) values (5,'Star Mozi','Debrecen', 180, 'nem', 'igen')
insert into Mozi(moziID, nev, varos, ferohely, vane3D, vanefelirat) values (6,'Dobó István Mozi','Eger', 250, 'nem', 'nem')

insert into Bufe (termekID, nev, tipus, ar, allergen, homerseklet) values (1, 'Popcorn','étel',800,2,'hideg');
insert into Bufe (termekID, nev, tipus, ar, allergen, homerseklet) values (2, 'Nachos','étel',1100,3,'hideg');
insert into Bufe (termekID, nev, tipus, ar, allergen, homerseklet) values (3, 'Kóla','ital',300,1,'hideg');
insert into Bufe (termekID, nev, tipus, ar, allergen, homerseklet) values (4, 'Jegestea','ital',250,1,'hideg');
insert into Bufe (termekID, nev, tipus, ar, allergen, homerseklet) values (5, 'Gumicukor','étel',400,5,'hideg');
insert into Bufe (termekID, nev, tipus, ar, allergen, homerseklet) values (6, 'Diákcsemege','étel',450,4,'hideg');
insert into Bufe (termekID, nev, tipus, ar, allergen, homerseklet) values (7, 'Tea','ital',350,1, 'meleg');
insert into Bufe (termekID, nev, tipus, ar, allergen, homerseklet) values (8, 'Melegszendvics','étel',600,4,'meleg');


insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (1,'2019.10.22',138,345000);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (2,'2019.10.23',100,250000);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (3,'2019.10.24',200,500000);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (4,'2019.10.25',45,112500);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (5,'2019.10.26',170,425000);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (6,'2019.10.27',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (7,'2019.10.28',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (8,'2019.10.29',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (9,'2019.10.30',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (10,'2019.10.31',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (11,'2019.11.01',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (12,'2019.11.02',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (13,'2019.11.03',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (14,'2019.11.04',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (15,'2019.11.05',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (16,'2019.11.06',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (17,'2019.11.07',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (18,'2019.11.08',0,0);
insert into Mozimusor(mozimusorID,vetites_datum,nezoszam,bevetel) values (19,'2019.11.09',0,0);


insert into Kapcsolo(mozimusorID,filmID,moziID) values (1,4,1);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (2,7,2);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (3,1,3);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (4,12,4);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (5,3,5);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (6,7,6);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (7,11,1);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (8,8,2);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (9,8,3);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (10,9,4);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (11,1,5);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (12,4,6);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (13,2,1);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (14,5,2);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (15,10,3);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (16,6,4);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (17,12,5);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (18,2,6);
insert into Kapcsolo(mozimusorID,filmID,moziID) values (19,6,1);




