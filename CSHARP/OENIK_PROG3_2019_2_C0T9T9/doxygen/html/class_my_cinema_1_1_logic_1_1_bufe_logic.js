var class_my_cinema_1_1_logic_1_1_bufe_logic =
[
    [ "BufeLogic", "class_my_cinema_1_1_logic_1_1_bufe_logic.html#aa2f8997ebef4a7a0287b18d7f3a92dee", null ],
    [ "CreateFood", "class_my_cinema_1_1_logic_1_1_bufe_logic.html#a3298dda6991dc9bbfab0fea3cdc82a22", null ],
    [ "DeleteFood", "class_my_cinema_1_1_logic_1_1_bufe_logic.html#a3f89a52f3e64da46c1311cef5b2e7572", null ],
    [ "GetFoodByID", "class_my_cinema_1_1_logic_1_1_bufe_logic.html#afa9a93d5b3a219b156d32c180ccad88a", null ],
    [ "GetFoods", "class_my_cinema_1_1_logic_1_1_bufe_logic.html#a9fb5b3b73face67ce9c2b61a63b03884", null ],
    [ "UpdateAllergen", "class_my_cinema_1_1_logic_1_1_bufe_logic.html#ae12a5200e829537719b590b2adf667f8", null ],
    [ "UpdateAr", "class_my_cinema_1_1_logic_1_1_bufe_logic.html#a857add26244a85690474e76b993375d0", null ],
    [ "UpdateHomerseklet", "class_my_cinema_1_1_logic_1_1_bufe_logic.html#a0f9a91062daec7ff0fd3f26ec93d372d", null ]
];