var class_my_cinema_1_1_logic_1_1_film_logic =
[
    [ "FilmLogic", "class_my_cinema_1_1_logic_1_1_film_logic.html#ad23c9d134ec8bfe1f6fd88dc1d723fc7", null ],
    [ "CreateFilm", "class_my_cinema_1_1_logic_1_1_film_logic.html#a45404429672220e141fee4f9bac11c5c", null ],
    [ "DeleteFilm", "class_my_cinema_1_1_logic_1_1_film_logic.html#aaa383cbe9673d1e7da46bf229782a9db", null ],
    [ "GetFilmByID", "class_my_cinema_1_1_logic_1_1_film_logic.html#a37e095c0c18695e7616d79007ec3173e", null ],
    [ "GetFilms", "class_my_cinema_1_1_logic_1_1_film_logic.html#a71b9528cc0af721c8d085f500d4745e9", null ],
    [ "UpdateEv", "class_my_cinema_1_1_logic_1_1_film_logic.html#a57140441496ae3fb7ea5542160c95cd8", null ],
    [ "UpdateFoszereplo", "class_my_cinema_1_1_logic_1_1_film_logic.html#a003b4f1b42b1a4971a23b366e5daad7f", null ],
    [ "UpdateHossz", "class_my_cinema_1_1_logic_1_1_film_logic.html#aa8779a37f7b7f39268b3a4e561c7df58", null ]
];