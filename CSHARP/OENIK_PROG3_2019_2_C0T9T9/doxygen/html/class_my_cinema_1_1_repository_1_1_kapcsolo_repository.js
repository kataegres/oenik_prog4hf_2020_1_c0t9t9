var class_my_cinema_1_1_repository_1_1_kapcsolo_repository =
[
    [ "KapcsoloRepository", "class_my_cinema_1_1_repository_1_1_kapcsolo_repository.html#afa6a7d6d53f9e363d807664bf63d591a", null ],
    [ "DeleteConnectionByFilmID", "class_my_cinema_1_1_repository_1_1_kapcsolo_repository.html#a6aeda8c4ab5aeea653d76e5b037a39b5", null ],
    [ "DeleteConnectionByMoziID", "class_my_cinema_1_1_repository_1_1_kapcsolo_repository.html#a601456109f0c174e33b8bdf3a571acf5", null ],
    [ "DeleteConnectionByMozimusorID", "class_my_cinema_1_1_repository_1_1_kapcsolo_repository.html#aa509a49ffb988df57bc77f03f7d39503", null ],
    [ "GetAll", "class_my_cinema_1_1_repository_1_1_kapcsolo_repository.html#adaebe0039e49a1e9091581570aff2fe0", null ],
    [ "GetConnectionByFilmID", "class_my_cinema_1_1_repository_1_1_kapcsolo_repository.html#ad8f852d59efbd580cda02902c8ebaade", null ],
    [ "GetConnectionByMoziID", "class_my_cinema_1_1_repository_1_1_kapcsolo_repository.html#adaa7389db4574e908c089fe3308a4970", null ],
    [ "GetConnectionByMozimusorID", "class_my_cinema_1_1_repository_1_1_kapcsolo_repository.html#ad239916c2c85f33203ffa9a3485d66d6", null ]
];