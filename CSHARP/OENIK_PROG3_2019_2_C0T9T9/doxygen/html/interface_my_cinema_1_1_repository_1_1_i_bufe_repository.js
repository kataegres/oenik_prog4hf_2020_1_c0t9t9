var interface_my_cinema_1_1_repository_1_1_i_bufe_repository =
[
    [ "AddFood", "interface_my_cinema_1_1_repository_1_1_i_bufe_repository.html#aebc3ac22ed293f384e56c28461e05e13", null ],
    [ "DeleteFood", "interface_my_cinema_1_1_repository_1_1_i_bufe_repository.html#ad50620e41bc39222a5ca82d8a1691572", null ],
    [ "GetFoodByID", "interface_my_cinema_1_1_repository_1_1_i_bufe_repository.html#a82c7021a35ee259427aa70eb14c29628", null ],
    [ "LastID", "interface_my_cinema_1_1_repository_1_1_i_bufe_repository.html#acee150b152f44e20fd443fbf0cd80525", null ],
    [ "UpdateAllergen", "interface_my_cinema_1_1_repository_1_1_i_bufe_repository.html#a64b18d9bfbd915b42838fa4ca98e94fa", null ],
    [ "UpdateAr", "interface_my_cinema_1_1_repository_1_1_i_bufe_repository.html#adf454a5c6dbee093fab01951d61d67b3", null ],
    [ "UpdateHomerseklet", "interface_my_cinema_1_1_repository_1_1_i_bufe_repository.html#ac7e22a37a07701b84836093a4d10b1c4", null ]
];