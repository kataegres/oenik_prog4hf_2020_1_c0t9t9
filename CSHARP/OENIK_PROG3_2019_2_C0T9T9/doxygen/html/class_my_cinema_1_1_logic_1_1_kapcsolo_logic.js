var class_my_cinema_1_1_logic_1_1_kapcsolo_logic =
[
    [ "KapcsoloLogic", "class_my_cinema_1_1_logic_1_1_kapcsolo_logic.html#ab447571a7e7f8afeb5b98f76cf536c02", null ],
    [ "DeleteConnectionByFilmID", "class_my_cinema_1_1_logic_1_1_kapcsolo_logic.html#a805d89b8f2e23a7e6c9e72be4a756d96", null ],
    [ "DeleteConnectionByMoziID", "class_my_cinema_1_1_logic_1_1_kapcsolo_logic.html#af74abb717253caf1ffe0abb691ad6766", null ],
    [ "DeleteConnectionByMozimusorID", "class_my_cinema_1_1_logic_1_1_kapcsolo_logic.html#a2bdc0a6026bfd4c125e88ae9b0ec7381", null ],
    [ "GetAll", "class_my_cinema_1_1_logic_1_1_kapcsolo_logic.html#a3b53d0871ef8b9ac5aee61407dff3984", null ],
    [ "GetConnectionByFilmID", "class_my_cinema_1_1_logic_1_1_kapcsolo_logic.html#a3e334f12c54d4f67956cab5f17a81547", null ],
    [ "GetConnectionByMoziID", "class_my_cinema_1_1_logic_1_1_kapcsolo_logic.html#a561c5c9cf10a4f1effac13d4fb78011f", null ],
    [ "GetConnectionByMozimusorID", "class_my_cinema_1_1_logic_1_1_kapcsolo_logic.html#a754387615075c162c5eb60c595995d19", null ]
];