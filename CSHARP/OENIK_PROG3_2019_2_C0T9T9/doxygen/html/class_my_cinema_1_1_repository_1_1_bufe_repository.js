var class_my_cinema_1_1_repository_1_1_bufe_repository =
[
    [ "BufeRepository", "class_my_cinema_1_1_repository_1_1_bufe_repository.html#ac993c75d2a42eabd91befaade1d39c25", null ],
    [ "AddFood", "class_my_cinema_1_1_repository_1_1_bufe_repository.html#a34dc5461f8aee245b82f83ac9b648eae", null ],
    [ "DeleteFood", "class_my_cinema_1_1_repository_1_1_bufe_repository.html#ad50faf24a4cd7ce658d3fad10171dad0", null ],
    [ "GetAll", "class_my_cinema_1_1_repository_1_1_bufe_repository.html#af78c33896a384fe2f3aa67c623326eff", null ],
    [ "GetFoodByID", "class_my_cinema_1_1_repository_1_1_bufe_repository.html#abdad47ffd7e4b00f3d39dbbe22af8066", null ],
    [ "LastID", "class_my_cinema_1_1_repository_1_1_bufe_repository.html#a13478dc81404f3f5190144cc0868b4b6", null ],
    [ "UpdateAllergen", "class_my_cinema_1_1_repository_1_1_bufe_repository.html#af0bac1e647751b7abc1cdcbcd63f8161", null ],
    [ "UpdateAr", "class_my_cinema_1_1_repository_1_1_bufe_repository.html#a4c1b17fb4d6f3ba1e77953b45517a669", null ],
    [ "UpdateHomerseklet", "class_my_cinema_1_1_repository_1_1_bufe_repository.html#a4b99f3b3e46fe300b730a6ef6f6bb813", null ]
];