var searchData=
[
  ['ferohely_40',['ferohely',['../class_my_cinema_1_1_data_1_1_mozi.html#abc41157364842b1f285066537c8a4089',1,'MyCinema::Data::Mozi']]],
  ['film_41',['Film',['../class_my_cinema_1_1_data_1_1_film.html',1,'MyCinema.Data.Film'],['../class_my_cinema_1_1_data_1_1_kapcsolo.html#a18c329b538255bb58fb71ddf139861e4',1,'MyCinema.Data.Kapcsolo.Film()'],['../class_my_cinema_1_1_data_1_1_film.html#aa7dc7551862fa632810bf20bdcd26096',1,'MyCinema.Data.Film.Film()']]],
  ['filmid_42',['filmID',['../class_my_cinema_1_1_data_1_1_film.html#a84e66b9558f5d1d4100e847e28b92311',1,'MyCinema.Data.Film.filmID()'],['../class_my_cinema_1_1_data_1_1_kapcsolo.html#a25b84006696d4f6c998fdc1db4c47d48',1,'MyCinema.Data.Kapcsolo.filmID()']]],
  ['filmlogic_43',['FilmLogic',['../class_my_cinema_1_1_logic_1_1_film_logic.html',1,'MyCinema.Logic.FilmLogic'],['../class_my_cinema_1_1_logic_1_1_film_logic.html#ad23c9d134ec8bfe1f6fd88dc1d723fc7',1,'MyCinema.Logic.FilmLogic.FilmLogic()'],['../class_my_cinema_1_1_logic_1_1_film_logic.html#a91fecb3c5a5816b064120310a3d4af82',1,'MyCinema.Logic.FilmLogic.FilmLogic(IFilmRepository repo)']]],
  ['filmrepository_44',['FilmRepository',['../class_my_cinema_1_1_repository_1_1_film_repository.html',1,'MyCinema.Repository.FilmRepository'],['../class_my_cinema_1_1_repository_1_1_film_repository.html#ab70dc2c2d08b88e78299a423329949a8',1,'MyCinema.Repository.FilmRepository.FilmRepository()']]],
  ['films_45',['Films',['../class_my_cinema_1_1_data_1_1_my_cinema_database_entities.html#a65404f135cb0cb235a58feca44b04912',1,'MyCinema::Data::MyCinemaDatabaseEntities']]],
  ['fomenu_46',['FoMenu',['../class_my_cinema_1_1_program_1_1_menu.html#a1f1010851d9ba898db7d0881d8b82e97',1,'MyCinema::Program::Menu']]],
  ['foszereplo_47',['foszereplo',['../class_my_cinema_1_1_data_1_1_film.html#af7c7c4a6a396439821fd874041871e98',1,'MyCinema::Data::Film']]]
];
