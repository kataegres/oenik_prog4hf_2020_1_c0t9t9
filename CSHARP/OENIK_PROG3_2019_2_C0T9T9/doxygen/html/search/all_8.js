var searchData=
[
  ['ibufelogic_64',['IBufeLogic',['../interface_my_cinema_1_1_logic_1_1_i_bufe_logic.html',1,'MyCinema::Logic']]],
  ['ibuferepository_65',['IBufeRepository',['../interface_my_cinema_1_1_repository_1_1_i_bufe_repository.html',1,'MyCinema::Repository']]],
  ['ifilmlogic_66',['IFilmLogic',['../interface_my_cinema_1_1_logic_1_1_i_film_logic.html',1,'MyCinema::Logic']]],
  ['ifilmrepository_67',['IFilmRepository',['../interface_my_cinema_1_1_repository_1_1_i_film_repository.html',1,'MyCinema::Repository']]],
  ['ikapcsolorepository_68',['IKapcsoloRepository',['../interface_my_cinema_1_1_repository_1_1_i_kapcsolo_repository.html',1,'MyCinema::Repository']]],
  ['imozilogic_69',['IMoziLogic',['../interface_my_cinema_1_1_logic_1_1_i_mozi_logic.html',1,'MyCinema::Logic']]],
  ['imozimusorlogic_70',['IMozimusorLogic',['../interface_my_cinema_1_1_logic_1_1_i_mozimusor_logic.html',1,'MyCinema::Logic']]],
  ['imozimusorrepository_71',['IMozimusorRepository',['../interface_my_cinema_1_1_repository_1_1_i_mozimusor_repository.html',1,'MyCinema::Repository']]],
  ['imozirepository_72',['IMoziRepository',['../interface_my_cinema_1_1_repository_1_1_i_mozi_repository.html',1,'MyCinema::Repository']]],
  ['init_73',['Init',['../class_my_cinema_1_1_logic_1_1_tests_1_1_cinema_tester.html#a39692f2caef66cc032d1ff72a5ae45fc',1,'MyCinema::Logic::Tests::CinemaTester']]],
  ['irepository_74',['IRepository',['../interface_my_cinema_1_1_repository_1_1_i_repository.html',1,'MyCinema::Repository']]],
  ['irepository_3c_20bufe_20_3e_75',['IRepository&lt; Bufe &gt;',['../interface_my_cinema_1_1_repository_1_1_i_repository.html',1,'MyCinema::Repository']]],
  ['irepository_3c_20film_20_3e_76',['IRepository&lt; Film &gt;',['../interface_my_cinema_1_1_repository_1_1_i_repository.html',1,'MyCinema::Repository']]],
  ['irepository_3c_20kapcsolo_20_3e_77',['IRepository&lt; Kapcsolo &gt;',['../interface_my_cinema_1_1_repository_1_1_i_repository.html',1,'MyCinema::Repository']]],
  ['irepository_3c_20mozi_20_3e_78',['IRepository&lt; Mozi &gt;',['../interface_my_cinema_1_1_repository_1_1_i_repository.html',1,'MyCinema::Repository']]],
  ['irepository_3c_20mozimusor_20_3e_79',['IRepository&lt; Mozimusor &gt;',['../interface_my_cinema_1_1_repository_1_1_i_repository.html',1,'MyCinema::Repository']]]
];
