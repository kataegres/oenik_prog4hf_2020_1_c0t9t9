var searchData=
[
  ['ibufelogic_164',['IBufeLogic',['../interface_my_cinema_1_1_logic_1_1_i_bufe_logic.html',1,'MyCinema::Logic']]],
  ['ibuferepository_165',['IBufeRepository',['../interface_my_cinema_1_1_repository_1_1_i_bufe_repository.html',1,'MyCinema::Repository']]],
  ['ifilmlogic_166',['IFilmLogic',['../interface_my_cinema_1_1_logic_1_1_i_film_logic.html',1,'MyCinema::Logic']]],
  ['ifilmrepository_167',['IFilmRepository',['../interface_my_cinema_1_1_repository_1_1_i_film_repository.html',1,'MyCinema::Repository']]],
  ['ikapcsolorepository_168',['IKapcsoloRepository',['../interface_my_cinema_1_1_repository_1_1_i_kapcsolo_repository.html',1,'MyCinema::Repository']]],
  ['imozilogic_169',['IMoziLogic',['../interface_my_cinema_1_1_logic_1_1_i_mozi_logic.html',1,'MyCinema::Logic']]],
  ['imozimusorlogic_170',['IMozimusorLogic',['../interface_my_cinema_1_1_logic_1_1_i_mozimusor_logic.html',1,'MyCinema::Logic']]],
  ['imozimusorrepository_171',['IMozimusorRepository',['../interface_my_cinema_1_1_repository_1_1_i_mozimusor_repository.html',1,'MyCinema::Repository']]],
  ['imozirepository_172',['IMoziRepository',['../interface_my_cinema_1_1_repository_1_1_i_mozi_repository.html',1,'MyCinema::Repository']]],
  ['irepository_173',['IRepository',['../interface_my_cinema_1_1_repository_1_1_i_repository.html',1,'MyCinema::Repository']]],
  ['irepository_3c_20bufe_20_3e_174',['IRepository&lt; Bufe &gt;',['../interface_my_cinema_1_1_repository_1_1_i_repository.html',1,'MyCinema::Repository']]],
  ['irepository_3c_20film_20_3e_175',['IRepository&lt; Film &gt;',['../interface_my_cinema_1_1_repository_1_1_i_repository.html',1,'MyCinema::Repository']]],
  ['irepository_3c_20kapcsolo_20_3e_176',['IRepository&lt; Kapcsolo &gt;',['../interface_my_cinema_1_1_repository_1_1_i_repository.html',1,'MyCinema::Repository']]],
  ['irepository_3c_20mozi_20_3e_177',['IRepository&lt; Mozi &gt;',['../interface_my_cinema_1_1_repository_1_1_i_repository.html',1,'MyCinema::Repository']]],
  ['irepository_3c_20mozimusor_20_3e_178',['IRepository&lt; Mozimusor &gt;',['../interface_my_cinema_1_1_repository_1_1_i_repository.html',1,'MyCinema::Repository']]]
];
