var hierarchy =
[
    [ "MyCinema.Data.Bufe", "class_my_cinema_1_1_data_1_1_bufe.html", null ],
    [ "MyCinema.Logic.Tests.Class1", "class_my_cinema_1_1_logic_1_1_tests_1_1_class1.html", null ],
    [ "DbContext", null, [
      [ "MyCinema.Data::MyCinemaDatabaseEntities", "class_my_cinema_1_1_data_1_1_my_cinema_database_entities.html", null ]
    ] ],
    [ "MyCinema.Data.Film", "class_my_cinema_1_1_data_1_1_film.html", null ],
    [ "MyCinema.Logic.FilmLogic", "class_my_cinema_1_1_logic_1_1_film_logic.html", null ],
    [ "MyCinema.Logic.IBufeLogic", "interface_my_cinema_1_1_logic_1_1_i_bufe_logic.html", [
      [ "MyCinema.Logic.BufeLogic", "class_my_cinema_1_1_logic_1_1_bufe_logic.html", null ]
    ] ],
    [ "MyCinema.Repository.IRepository< T >", "interface_my_cinema_1_1_repository_1_1_i_repository.html", null ],
    [ "MyCinema.Repository.IRepository< Bufe >", "interface_my_cinema_1_1_repository_1_1_i_repository.html", [
      [ "MyCinema.Repository.IBufeRepository", "interface_my_cinema_1_1_repository_1_1_i_bufe_repository.html", [
        [ "MyCinema.Repository.BufeRepository", "class_my_cinema_1_1_repository_1_1_bufe_repository.html", null ]
      ] ]
    ] ],
    [ "MyCinema.Repository.IRepository< Film >", "interface_my_cinema_1_1_repository_1_1_i_repository.html", [
      [ "MyCinema.Repository.IFilmRepository", "interface_my_cinema_1_1_repository_1_1_i_film_repository.html", [
        [ "MyCinema.Repository.FilmRepository", "class_my_cinema_1_1_repository_1_1_film_repository.html", null ]
      ] ]
    ] ],
    [ "MyCinema.Repository.IRepository< Kapcsolo >", "interface_my_cinema_1_1_repository_1_1_i_repository.html", [
      [ "MyCinema.Repository.IKapcsoloRepository", "interface_my_cinema_1_1_repository_1_1_i_kapcsolo_repository.html", [
        [ "MyCinema.Repository.KapcsoloRepository", "class_my_cinema_1_1_repository_1_1_kapcsolo_repository.html", null ]
      ] ]
    ] ],
    [ "MyCinema.Repository.IRepository< Mozi >", "interface_my_cinema_1_1_repository_1_1_i_repository.html", [
      [ "MyCinema.Repository.IMoziRepository", "interface_my_cinema_1_1_repository_1_1_i_mozi_repository.html", [
        [ "MyCinema.Repository.MoziRepository", "class_my_cinema_1_1_repository_1_1_mozi_repository.html", null ]
      ] ]
    ] ],
    [ "MyCinema.Repository.IRepository< Mozimusor >", "interface_my_cinema_1_1_repository_1_1_i_repository.html", [
      [ "MyCinema.Repository.IMozimusorRepository", "interface_my_cinema_1_1_repository_1_1_i_mozimusor_repository.html", [
        [ "MyCinema.Repository.MozimusorRepository", "class_my_cinema_1_1_repository_1_1_mozimusor_repository.html", null ]
      ] ]
    ] ],
    [ "MyCinema.Data.Kapcsolo", "class_my_cinema_1_1_data_1_1_kapcsolo.html", null ],
    [ "MyCinema.Logic.KapcsoloLogic", "class_my_cinema_1_1_logic_1_1_kapcsolo_logic.html", null ],
    [ "MyCinema.Program.Menu", "class_my_cinema_1_1_program_1_1_menu.html", null ],
    [ "MyCinema.Data.Mozi", "class_my_cinema_1_1_data_1_1_mozi.html", null ],
    [ "MyCinema.Logic.MoziLogic", "class_my_cinema_1_1_logic_1_1_mozi_logic.html", null ],
    [ "MyCinema.Data.Mozimusor", "class_my_cinema_1_1_data_1_1_mozimusor.html", null ],
    [ "MyCinema.Logic.MozimusorLogic", "class_my_cinema_1_1_logic_1_1_mozimusor_logic.html", null ]
];