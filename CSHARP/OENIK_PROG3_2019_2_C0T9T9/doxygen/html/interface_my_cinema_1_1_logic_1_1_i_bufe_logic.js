var interface_my_cinema_1_1_logic_1_1_i_bufe_logic =
[
    [ "CreateFood", "interface_my_cinema_1_1_logic_1_1_i_bufe_logic.html#ae58a4ce23a6fe1b5be8db1baada7e770", null ],
    [ "DeleteFood", "interface_my_cinema_1_1_logic_1_1_i_bufe_logic.html#adf2602450878cc82a05ce48d285d9260", null ],
    [ "GetFoodByID", "interface_my_cinema_1_1_logic_1_1_i_bufe_logic.html#a3756b90a09551325e85c0e440796bc5e", null ],
    [ "GetFoods", "interface_my_cinema_1_1_logic_1_1_i_bufe_logic.html#af6b731038e4b0fb87fd895edc6ebfbbb", null ],
    [ "UpdateAllergen", "interface_my_cinema_1_1_logic_1_1_i_bufe_logic.html#abb0aef18da2c2cbc77102ace419092b4", null ],
    [ "UpdateAr", "interface_my_cinema_1_1_logic_1_1_i_bufe_logic.html#a1b24213fd761c99340c8da23d1d567b1", null ],
    [ "UpdateHomerseklet", "interface_my_cinema_1_1_logic_1_1_i_bufe_logic.html#a25156ebef5e19c62f7fff7987e66a0bb", null ]
];