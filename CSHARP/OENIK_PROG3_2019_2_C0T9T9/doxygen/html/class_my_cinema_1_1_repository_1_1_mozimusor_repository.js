var class_my_cinema_1_1_repository_1_1_mozimusor_repository =
[
    [ "MozimusorRepository", "class_my_cinema_1_1_repository_1_1_mozimusor_repository.html#ab6942cc1ed569cca97822b527dbaa361", null ],
    [ "AddMozimusor", "class_my_cinema_1_1_repository_1_1_mozimusor_repository.html#a88c9452faca07d2a828cd10150fa5a50", null ],
    [ "DeleteMusor", "class_my_cinema_1_1_repository_1_1_mozimusor_repository.html#a7acd3e1c74ef79876338368cdb7d2c37", null ],
    [ "GetAll", "class_my_cinema_1_1_repository_1_1_mozimusor_repository.html#abff488193e7b924158d757c72406a02e", null ],
    [ "GetMozimusorByID", "class_my_cinema_1_1_repository_1_1_mozimusor_repository.html#ac4abad2b293412372c8eb32266b68b9b", null ],
    [ "LastID", "class_my_cinema_1_1_repository_1_1_mozimusor_repository.html#a4c34ad7d330b62d83f21705b59f8cad0", null ],
    [ "UpdateBevetel", "class_my_cinema_1_1_repository_1_1_mozimusor_repository.html#a1fd6e02cb0ccf506c3d0d20ef6335131", null ],
    [ "UpdateNezoszam", "class_my_cinema_1_1_repository_1_1_mozimusor_repository.html#a5fad459fb1c461df26142a77e2b20dc1", null ]
];