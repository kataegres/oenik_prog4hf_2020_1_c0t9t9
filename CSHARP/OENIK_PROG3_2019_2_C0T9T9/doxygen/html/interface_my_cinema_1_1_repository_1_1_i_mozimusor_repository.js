var interface_my_cinema_1_1_repository_1_1_i_mozimusor_repository =
[
    [ "AddMozimusor", "interface_my_cinema_1_1_repository_1_1_i_mozimusor_repository.html#aa98b2a52f2c3bb836d3f643ff09a44ef", null ],
    [ "DeleteMusor", "interface_my_cinema_1_1_repository_1_1_i_mozimusor_repository.html#a291599c870e8ead57dabf10b73b213bc", null ],
    [ "GetMozimusorByID", "interface_my_cinema_1_1_repository_1_1_i_mozimusor_repository.html#aae1ed583d0eea2aa971043ec00646745", null ],
    [ "LastID", "interface_my_cinema_1_1_repository_1_1_i_mozimusor_repository.html#a8e769097772c635902a4e3ca842176c5", null ],
    [ "UpdateBevetel", "interface_my_cinema_1_1_repository_1_1_i_mozimusor_repository.html#a86524be6d360c2450b04653e1298e8c9", null ],
    [ "UpdateNezoszam", "interface_my_cinema_1_1_repository_1_1_i_mozimusor_repository.html#ad94a28d807b41b5efda175b29053af17", null ]
];