var class_my_cinema_1_1_logic_1_1_mozi_logic =
[
    [ "MoziLogic", "class_my_cinema_1_1_logic_1_1_mozi_logic.html#ae71c8fde055a75f2fd0e1f501791b908", null ],
    [ "CreateMozi", "class_my_cinema_1_1_logic_1_1_mozi_logic.html#a151cdaa6455fc2fa7754ea2a8c5bbadd", null ],
    [ "DeleteMozi", "class_my_cinema_1_1_logic_1_1_mozi_logic.html#a3e584d6e4eaf30cd3f73f93f3582e5fa", null ],
    [ "GetMoziByID", "class_my_cinema_1_1_logic_1_1_mozi_logic.html#a5574c97aad172210f2119a46aa29ac46", null ],
    [ "GetMozis", "class_my_cinema_1_1_logic_1_1_mozi_logic.html#a1aa0bfea0842bc3322b834bfef198e8a", null ],
    [ "Update3D", "class_my_cinema_1_1_logic_1_1_mozi_logic.html#a64b2c150f92949ff88f173d06bffe8ae", null ],
    [ "UpdateFelirat", "class_my_cinema_1_1_logic_1_1_mozi_logic.html#aee027152b2d354589f907ffc0de6d6d7", null ],
    [ "UpdateFerohely", "class_my_cinema_1_1_logic_1_1_mozi_logic.html#ad68d2c08eb46a98e8f6abacc96c0a0f3", null ]
];