var class_my_cinema_1_1_logic_1_1_mozimusor_logic =
[
    [ "MozimusorLogic", "class_my_cinema_1_1_logic_1_1_mozimusor_logic.html#a4df55db60e9408e0c51517d7b17d72e8", null ],
    [ "CreateMozimusor", "class_my_cinema_1_1_logic_1_1_mozimusor_logic.html#a77a774188386da1fe15b60122c2d6b7c", null ],
    [ "DeleteMozimusor", "class_my_cinema_1_1_logic_1_1_mozimusor_logic.html#a19697f5ebfe106a41826275f7b57ab8a", null ],
    [ "GetMozimusorByID", "class_my_cinema_1_1_logic_1_1_mozimusor_logic.html#a9a3795a5c759b84b59aa640cbceb6d83", null ],
    [ "GetMozimusors", "class_my_cinema_1_1_logic_1_1_mozimusor_logic.html#a7968a524903d57ff6e2f12fcdcc04f7d", null ],
    [ "UpdateBevetel", "class_my_cinema_1_1_logic_1_1_mozimusor_logic.html#a87211042c3ff88d4fa1b05718679de5e", null ],
    [ "UpdateNezoszam", "class_my_cinema_1_1_logic_1_1_mozimusor_logic.html#ab9358aa2be7fcbdc09536ef5cec10aa5", null ]
];