var namespace_my_cinema_1_1_logic =
[
    [ "Tests", "namespace_my_cinema_1_1_logic_1_1_tests.html", "namespace_my_cinema_1_1_logic_1_1_tests" ],
    [ "BufeLogic", "class_my_cinema_1_1_logic_1_1_bufe_logic.html", "class_my_cinema_1_1_logic_1_1_bufe_logic" ],
    [ "FilmLogic", "class_my_cinema_1_1_logic_1_1_film_logic.html", "class_my_cinema_1_1_logic_1_1_film_logic" ],
    [ "IBufeLogic", "interface_my_cinema_1_1_logic_1_1_i_bufe_logic.html", "interface_my_cinema_1_1_logic_1_1_i_bufe_logic" ],
    [ "KapcsoloLogic", "class_my_cinema_1_1_logic_1_1_kapcsolo_logic.html", "class_my_cinema_1_1_logic_1_1_kapcsolo_logic" ],
    [ "MoziLogic", "class_my_cinema_1_1_logic_1_1_mozi_logic.html", "class_my_cinema_1_1_logic_1_1_mozi_logic" ],
    [ "MozimusorLogic", "class_my_cinema_1_1_logic_1_1_mozimusor_logic.html", "class_my_cinema_1_1_logic_1_1_mozimusor_logic" ]
];