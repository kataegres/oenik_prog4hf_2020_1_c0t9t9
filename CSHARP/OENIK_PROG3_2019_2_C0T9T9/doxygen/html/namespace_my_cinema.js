var namespace_my_cinema =
[
    [ "Data", "namespace_my_cinema_1_1_data.html", "namespace_my_cinema_1_1_data" ],
    [ "Logic", "namespace_my_cinema_1_1_logic.html", "namespace_my_cinema_1_1_logic" ],
    [ "Program", "namespace_my_cinema_1_1_program.html", "namespace_my_cinema_1_1_program" ],
    [ "Repository", "namespace_my_cinema_1_1_repository.html", "namespace_my_cinema_1_1_repository" ]
];