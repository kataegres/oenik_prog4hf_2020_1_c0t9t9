var class_my_cinema_1_1_repository_1_1_mozi_repository =
[
    [ "MoziRepository", "class_my_cinema_1_1_repository_1_1_mozi_repository.html#af0ac1e04e30a1636b6772de482fafb4f", null ],
    [ "AddMozi", "class_my_cinema_1_1_repository_1_1_mozi_repository.html#ae9975e7e1c86c589542b35c8d2fe27d5", null ],
    [ "DeleteMozi", "class_my_cinema_1_1_repository_1_1_mozi_repository.html#a7f3fac84331650a4d943c7eddbfd7fb2", null ],
    [ "GetAll", "class_my_cinema_1_1_repository_1_1_mozi_repository.html#a2b24f671b1665e1b0dd3fa300cf28d5a", null ],
    [ "GetMoziByID", "class_my_cinema_1_1_repository_1_1_mozi_repository.html#afa33fdfea6e27c4bbe355e10ce9f0606", null ],
    [ "LastID", "class_my_cinema_1_1_repository_1_1_mozi_repository.html#af12a236d9f6c6192b01d83d9cdad2da1", null ],
    [ "Update3D", "class_my_cinema_1_1_repository_1_1_mozi_repository.html#a8204b03fb635de2dce5076c4a5d324e4", null ],
    [ "UpdateFelirat", "class_my_cinema_1_1_repository_1_1_mozi_repository.html#aa287699fd45356601778fb2c835e63cd", null ],
    [ "UpdateFerohely", "class_my_cinema_1_1_repository_1_1_mozi_repository.html#a338b8a76cba334311ee01775ac57485a", null ]
];