var class_my_cinema_1_1_repository_1_1_film_repository =
[
    [ "FilmRepository", "class_my_cinema_1_1_repository_1_1_film_repository.html#ab70dc2c2d08b88e78299a423329949a8", null ],
    [ "AddFilm", "class_my_cinema_1_1_repository_1_1_film_repository.html#a84f17d491bede8fa22e5e8ab78c3d749", null ],
    [ "DeleteFilm", "class_my_cinema_1_1_repository_1_1_film_repository.html#aeb2e5388b2bd6d18c1ba25dcf2fa86c6", null ],
    [ "GetAll", "class_my_cinema_1_1_repository_1_1_film_repository.html#a72e17ea6eeb67faf35dc54b641783da5", null ],
    [ "GetFilmByID", "class_my_cinema_1_1_repository_1_1_film_repository.html#ac4b5295bd1c3de1af74529e6cde5a05a", null ],
    [ "LastID", "class_my_cinema_1_1_repository_1_1_film_repository.html#a8fafd67cb1c870d74855a8e73975f4e3", null ],
    [ "UpdateEv", "class_my_cinema_1_1_repository_1_1_film_repository.html#a2a1d9e6e8a497000d9a2e5d750696e30", null ],
    [ "UpdateFoszereplo", "class_my_cinema_1_1_repository_1_1_film_repository.html#a1c3b7235e40a756b4306f4c90c75a014", null ],
    [ "UpdateHossz", "class_my_cinema_1_1_repository_1_1_film_repository.html#ab698db7681bf6a4a91314c2f054c0f12", null ]
];