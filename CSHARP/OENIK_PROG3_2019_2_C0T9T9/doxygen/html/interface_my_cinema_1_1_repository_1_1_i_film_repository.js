var interface_my_cinema_1_1_repository_1_1_i_film_repository =
[
    [ "AddFilm", "interface_my_cinema_1_1_repository_1_1_i_film_repository.html#ad39a68489396876df69a8f4b109e7e8f", null ],
    [ "DeleteFilm", "interface_my_cinema_1_1_repository_1_1_i_film_repository.html#a56091be1e9b2e3ff9a6b0284e58c9631", null ],
    [ "GetFilmByID", "interface_my_cinema_1_1_repository_1_1_i_film_repository.html#abc89c217a51f2d70f0ca30bf1acf02cc", null ],
    [ "LastID", "interface_my_cinema_1_1_repository_1_1_i_film_repository.html#a1f3230e662b7ef8cf361102f427c0603", null ],
    [ "UpdateEv", "interface_my_cinema_1_1_repository_1_1_i_film_repository.html#a60f445b5d1d76ede6fd71230bb6a4a4c", null ],
    [ "UpdateFoszereplo", "interface_my_cinema_1_1_repository_1_1_i_film_repository.html#a6227fa05b312fdf68afe3eb54ff5a487", null ],
    [ "UpdateHossz", "interface_my_cinema_1_1_repository_1_1_i_film_repository.html#a8c505b649f5c9273cdd94662df3fbafc", null ]
];