var namespace_my_cinema_1_1_repository =
[
    [ "BufeRepository", "class_my_cinema_1_1_repository_1_1_bufe_repository.html", "class_my_cinema_1_1_repository_1_1_bufe_repository" ],
    [ "FilmRepository", "class_my_cinema_1_1_repository_1_1_film_repository.html", "class_my_cinema_1_1_repository_1_1_film_repository" ],
    [ "IBufeRepository", "interface_my_cinema_1_1_repository_1_1_i_bufe_repository.html", "interface_my_cinema_1_1_repository_1_1_i_bufe_repository" ],
    [ "IFilmRepository", "interface_my_cinema_1_1_repository_1_1_i_film_repository.html", "interface_my_cinema_1_1_repository_1_1_i_film_repository" ],
    [ "IKapcsoloRepository", "interface_my_cinema_1_1_repository_1_1_i_kapcsolo_repository.html", "interface_my_cinema_1_1_repository_1_1_i_kapcsolo_repository" ],
    [ "IMozimusorRepository", "interface_my_cinema_1_1_repository_1_1_i_mozimusor_repository.html", "interface_my_cinema_1_1_repository_1_1_i_mozimusor_repository" ],
    [ "IMoziRepository", "interface_my_cinema_1_1_repository_1_1_i_mozi_repository.html", "interface_my_cinema_1_1_repository_1_1_i_mozi_repository" ],
    [ "IRepository", "interface_my_cinema_1_1_repository_1_1_i_repository.html", "interface_my_cinema_1_1_repository_1_1_i_repository" ],
    [ "KapcsoloRepository", "class_my_cinema_1_1_repository_1_1_kapcsolo_repository.html", "class_my_cinema_1_1_repository_1_1_kapcsolo_repository" ],
    [ "MozimusorRepository", "class_my_cinema_1_1_repository_1_1_mozimusor_repository.html", "class_my_cinema_1_1_repository_1_1_mozimusor_repository" ],
    [ "MoziRepository", "class_my_cinema_1_1_repository_1_1_mozi_repository.html", "class_my_cinema_1_1_repository_1_1_mozi_repository" ]
];