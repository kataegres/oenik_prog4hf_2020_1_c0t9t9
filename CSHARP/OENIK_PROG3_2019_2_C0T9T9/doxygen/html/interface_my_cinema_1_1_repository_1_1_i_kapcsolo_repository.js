var interface_my_cinema_1_1_repository_1_1_i_kapcsolo_repository =
[
    [ "DeleteConnectionByFilmID", "interface_my_cinema_1_1_repository_1_1_i_kapcsolo_repository.html#a49deb16f231fbc38cf225e9eea174fbd", null ],
    [ "DeleteConnectionByMoziID", "interface_my_cinema_1_1_repository_1_1_i_kapcsolo_repository.html#a0a49e14cf849518b279089fb8dc79ff7", null ],
    [ "DeleteConnectionByMozimusorID", "interface_my_cinema_1_1_repository_1_1_i_kapcsolo_repository.html#afdb2706dd5bbb5077e0d669cd731e4b4", null ],
    [ "GetConnectionByFilmID", "interface_my_cinema_1_1_repository_1_1_i_kapcsolo_repository.html#a90bff44661a69859f53b6fac00a3fed8", null ],
    [ "GetConnectionByMoziID", "interface_my_cinema_1_1_repository_1_1_i_kapcsolo_repository.html#a5a116ae914a291c6290689a27d92a689", null ],
    [ "GetConnectionByMozimusorID", "interface_my_cinema_1_1_repository_1_1_i_kapcsolo_repository.html#acd8d4a7b12cf045e96dc442d1aae9804", null ]
];