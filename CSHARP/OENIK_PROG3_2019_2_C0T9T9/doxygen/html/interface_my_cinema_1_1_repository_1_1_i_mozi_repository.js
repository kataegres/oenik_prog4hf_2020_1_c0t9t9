var interface_my_cinema_1_1_repository_1_1_i_mozi_repository =
[
    [ "AddMozi", "interface_my_cinema_1_1_repository_1_1_i_mozi_repository.html#a2e3969884422e147ca588645256fd2a5", null ],
    [ "DeleteMozi", "interface_my_cinema_1_1_repository_1_1_i_mozi_repository.html#ab44227cb329cfd1a4f95360fd82c1f93", null ],
    [ "GetMoziByID", "interface_my_cinema_1_1_repository_1_1_i_mozi_repository.html#a95c48daa245ccae9bda615359241d80e", null ],
    [ "LastID", "interface_my_cinema_1_1_repository_1_1_i_mozi_repository.html#a50c9854bf081d9a7097ceeba0f5bedf1", null ],
    [ "Update3D", "interface_my_cinema_1_1_repository_1_1_i_mozi_repository.html#a8e23f21d63dc41aaab295f0d83bcb7df", null ],
    [ "UpdateFelirat", "interface_my_cinema_1_1_repository_1_1_i_mozi_repository.html#ae1357d9d25e310d7b974fe3767982e5f", null ],
    [ "UpdateFerohely", "interface_my_cinema_1_1_repository_1_1_i_mozi_repository.html#ab2f83e4d4788ec46747e0443a11a9856", null ]
];