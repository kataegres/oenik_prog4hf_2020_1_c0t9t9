﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using MyCinema.Data;
    using MyCinema.Logic;

    /// <summary>
    /// Menu class.
    /// </summary>
    public class Menu
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// Constructor.
        /// </summary>
        public Menu()
        {
            Exit = false;
            this.Db = new MyCinemaDatabaseEntities();
        }

        private static bool Exit { get; set; }

        private static string First { get; set; }

        private static string Second { get; set; }

        private static string Third { get; set; }

        private MyCinemaDatabaseEntities Db { get; set; }

        /// <summary>
        /// Main menu method.
        /// </summary>
        public void FoMenu()
        {
            do
            {
                string fo = "SYMPHONY CINEMA";
                Console.Write("- WELCOME TO THE DATABASE OF ");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write(fo);
                Console.ResetColor();
                Console.WriteLine("! --");

                Console.WriteLine();

                Console.WriteLine("--------- PLEASE CHOOSE A NUMBER! --------------");
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine("--------------- 1. CREATE   --------------------");
                Console.WriteLine("--------------- 2. READ ONE --------------------");
                Console.WriteLine("--------------- 3. UPDATE   --------------------");
                Console.WriteLine("--------------- 4. DELETE   --------------------");
                Console.WriteLine("--------------- 5. LIST ALL --------------------");
                Console.WriteLine("--------------- 6. OTHER    --------------------");
                Console.WriteLine("--------------- 7. EXIT     --------------------");
                Console.WriteLine("------------------------------------------------");
                First = Console.ReadLine();
                Console.Clear();

                if (First == "1" || First == "2" || First == "3" || First == "4" || First == "5")
                {
                    this.CRUD();
                }
                else if (First == "6")
                {
                    this.OTHER();
                }
                else if (First == "7")
                {
                    Exit = true;
                }
                else
                {
                    Console.WriteLine("PLEASE CHOOSE FROM THE NUMBERS BELOW.");
                }
            }
            while (Exit != true);

            Console.WriteLine("BYE!");
            Thread.Sleep(500);
            System.Environment.Exit(1);
        }

        /// <summary>
        /// Second menu method, which follows the main menu + CRUD methods.
        /// </summary>
        public void CRUD()
        {
        Start:
            Console.WriteLine("--------- PLEASE CHOOSE A TABLE! --------------");
            Console.WriteLine("-----------------------------------------------");
            Console.WriteLine("---------------- 1. BÜFÉ      -----------------");
            Console.WriteLine("---------------- 2. FILM      -----------------");
            Console.WriteLine("---------------- 3. MOZI      -----------------");
            Console.WriteLine("---------------- 4. MOZIMŰSOR -----------------");
            Console.WriteLine("---------------- 5. BACK      -----------------");
            Console.WriteLine("---------------- 6. EXIT      -----------------");
            Console.WriteLine("-----------------------------------------------");
            Second = Console.ReadLine();
            Console.Clear();

            if (First == "1")
            {
                switch (Second)
                {
                    case "1":
                        this.CreateFood();
                        break;
                    case "2":
                        this.CreateFilm();
                        break;
                    case "3":
                        this.CreateCinema();
                        break;
                    case "4":
                        this.CreateProgramme();
                        break;
                    case "5":
                        Console.WriteLine("GOING BACK");
                        Thread.Sleep(1000);
                        Console.Clear();
                        this.FoMenu();
                        break;
                    case "6":
                        Console.WriteLine("BYE!");
                        Thread.Sleep(500);
                        System.Environment.Exit(1);
                        break;
                    default:
                        Console.WriteLine("CHOOSE FROM THE NUMBERS BELOW.");
                        goto Start;
                }
            }
            else if (First == "2")
            {
                switch (Second)
                {
                    case "1":
                        this.ReadFood();
                        break;
                    case "2":
                        this.ReadFilm();
                        break;
                    case "3":
                        this.ReadCinema();
                        break;
                    case "4":
                        this.ReadProgramme();
                        break;
                    case "5":
                        Console.WriteLine("GOING BACK");
                        Thread.Sleep(1000);
                        Console.Clear();
                        this.FoMenu();
                        break;
                    case "6":
                        Console.WriteLine("BYE!");
                        Thread.Sleep(500);
                        System.Environment.Exit(1);
                        break;
                    default:
                        Console.WriteLine("CHOOSE FROM THE NUMBERS BELOW.");
                        goto Start;
                }
            }
            else if (First == "3")
            {
                switch (Second)
                {
                    case "1":
                    ujra:
                        Console.WriteLine("---------------- 1. UPDATE PRICE        -----------------");
                        Console.WriteLine("---------------- 2. UPDATE ALLERGENS    -----------------");
                        Console.WriteLine("---------------- 3. UPDATE TEMPERATURE  -----------------");
                        Third = Console.ReadLine();
                        if (Third == "1")
                        {
                            this.UpdatePrice();
                        }
                        else if (Third == "2")
                        {
                            this.UpdateAllerg();
                        }
                        else if (Third == "3")
                        {
                            this.UpdateTemp();
                        }
                        else
                        {
                            Console.WriteLine("CHOOSE FROM THE NUMBERS BELOW.");
                            goto ujra;
                        }

                        break;
                    case "2":
                        Console.WriteLine("---------------- 1. UPDATE LENGTH          -------------");
                        Console.WriteLine("---------------- 2. UPDATE RELEASE YEAR    -------------");
                        Console.WriteLine("---------------- 3. UPDATE MAIN ACTOR      -------------");
                        Third = Console.ReadLine();
                        if (Third == "1")
                        {
                            this.UpdateLength();
                        }
                        else if (Third == "2")
                        {
                            this.UpdateYear();
                        }
                        else if (Third == "3")
                        {
                            this.UpdateActor();
                        }
                        else
                        {
                            Console.WriteLine("CHOOSE FROM THE NUMBERS BELOW.");
                            goto ujra;
                        }

                        break;
                    case "3":
                        Console.WriteLine("---------------- 1. UPDATE 3D         -------------");
                        Console.WriteLine("---------------- 2. UPDATE SUBTITLES  -------------");
                        Console.WriteLine("---------------- 3. UPDATE SEATS      -------------");
                        Third = Console.ReadLine();
                        if (Third == "1")
                        {
                            this.Update3D();
                        }
                        else if (Third == "2")
                        {
                            this.UpdateSubs();
                        }
                        else if (Third == "3")
                        {
                            this.UpdateSeats();
                        }
                        else
                        {
                            Console.WriteLine("CHOOSE FROM THE NUMBERS BELOW.");
                            goto ujra;
                        }

                        break;
                    case "4":
                        Console.WriteLine("---------------- 1. UPDATE INCOME    -------------");
                        Console.WriteLine("---------------- 2. UPDATE AUDIENCE  -------------");
                        Third = Console.ReadLine();
                        if (Third == "1")
                        {
                            this.UpdateIncome();
                        }
                        else if (Third == "2")
                        {
                            this.UpdateAudience();
                        }
                        else
                        {
                            Console.WriteLine("CHOOSE FROM THE NUMBERS BELOW.");
                            goto ujra;
                        }

                        break;
                    case "5":
                        Console.WriteLine("GOING BACK");
                        Thread.Sleep(1000);
                        Console.Clear();
                        this.FoMenu();
                        break;
                    case "6":
                        Console.WriteLine("BYE!");
                        Thread.Sleep(500);
                        System.Environment.Exit(1);
                        break;
                    default:
                        Console.WriteLine("CHOOSE FROM THE NUMBERS BELOW.");
                        goto Start;
                }
            }
            else if (First == "4")
            {
                try
                {
                    switch (Second)
                    {
                        case "1":
                            this.DeleteFood();
                            break;
                        case "2":
                            this.DeleteFilm();
                            break;
                        case "3":
                            this.DeleteCinema();
                            break;
                        case "4":
                            this.DeleteProgramme();
                            break;
                        case "5":
                            Console.WriteLine("GOING BACK");
                            Thread.Sleep(1000);
                            Console.Clear();
                            this.FoMenu();
                            break;
                        case "6":
                            Console.WriteLine("BYE!");
                            Thread.Sleep(500);
                            System.Environment.Exit(1);
                            break;
                        default:
                            Console.WriteLine("CHOOSE FROM THE NUMBERS BELOW.");
                            goto Start;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine();
                }
            }
            else if (First == "5")
            {
                switch (Second)
                {
                    case "1":
                        this.ListFoods();
                        break;
                    case "2":
                        this.ListFilms();
                        break;
                    case "3":
                        this.ListCinemas();
                        break;
                    case "4":
                        this.ListProgrammes();
                        break;
                    case "5":
                        Console.WriteLine("GOING BACK");
                        Thread.Sleep(1000);
                        Console.Clear();
                        this.FoMenu();
                        break;
                    case "6":
                        Console.WriteLine("BYE!");
                        Thread.Sleep(500);
                        System.Environment.Exit(1);
                        break;
                    default:
                        Console.WriteLine("CHOOSE FROM THE NUMBERS BELOW.");
                        goto Start;
                }
            }
        }

        /// <summary>
        /// Also a second menu method with the non-CRUD methods.
        /// </summary>
        public void OTHER()
        {
            Console.WriteLine("------ 1. CHECK THE BUFFET'S AVERAGE PRICE          -------");
            Console.WriteLine("------ 2. GET THE NEWEST MOVIE IN THE REPERTOIRE    -------");
            Console.WriteLine("------ 3. TOP 3 CINEMAS WITH MORE THAN 200 SEATS    -------");
            Console.WriteLine("------ 4. AUDIENCE COUNTER                          -------");
            Console.WriteLine("------ 5. MOVIE OF THE DAY                          -------");
            Second = Console.ReadLine();
            Console.Clear();

            switch (Second)
            {
                case "1":
                    this.AvgPrice();
                    break;
                case "2":
                    this.NewestMovie();
                    break;
                case "3":
                    this.Top3Cinemas();
                    break;
                case "4":
                    this.AudienceCounter();
                    break;
                case "5":
                    this.JavaConnection();
                    break;
            }
        }

        /// <summary>
        /// Creates a food object.
        /// </summary>
        private void CreateFood()
        {
            try
            {
                Console.Write("NAME: ");
                string name = Console.ReadLine();
                Console.Write("PRICE: ");
                int price = int.Parse(Console.ReadLine());
                Console.Write("TYPE: ");
                string type = Console.ReadLine();
                Console.Write("ALLERGENS: ");
                int allergens = int.Parse(Console.ReadLine());
                Console.Write("TEMPERATURE: ");
                string temp = Console.ReadLine();

                IBufeLogic bl = new BufeLogic();
                bl.CreateFood(new Bufe { nev = name, ar = price, tipus = type, allergen = allergens, homerseklet = temp });

                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("CREATION WAS SUCCESSFUL!");

                Console.WriteLine("--------------------------------------------------------------------");
                Console.WriteLine($"{name} || {price} HUF || {type} || allergének: {allergens} || {temp}");
                Console.WriteLine("--------------------------------------------------------------------");
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("CREATION FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
            }
        }

        /// <summary>
        /// Creates a film object.
        /// </summary>
        private void CreateFilm()
        {
            try
            {
                Console.Write("TITLE: ");
                string title = Console.ReadLine();
                Console.Write("GENRE: ");
                string genre = Console.ReadLine();
                Console.Write("LENGTH: ");
                int length = int.Parse(Console.ReadLine());
                Console.Write("RELEASE YEAR: ");
                int year = int.Parse(Console.ReadLine());
                Console.Write("MAIN ACTOR/ACTRESS: ");
                string actor = Console.ReadLine();

                IFilmLogic fl = new FilmLogic();
                fl.CreateFilm(new Film { cim = title, mufaj = genre, hossz = length, ev = year, foszereplo = actor });

                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("CREATION WAS SUCCESSFUL!");

                Console.WriteLine("---------------------------------------------------------------");
                Console.WriteLine($"{title} || {genre} || {length} mins || {year} || {actor}");
                Console.WriteLine("---------------------------------------------------------------");
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("CREATION FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
            }
        }

        /// <summary>
        /// Creates a cinema object.
        /// </summary>
        private void CreateCinema()
        {
            try
            {
                Console.Write("NAME: ");
                string name = Console.ReadLine();
                Console.Write("CITY: ");
                string city = Console.ReadLine();
                Console.Write("MAX SEATS: ");
                int max = int.Parse(Console.ReadLine());
                Console.Write("3D? ");
                string d = Console.ReadLine();
                Console.Write("SUBS? ");
                string sub = Console.ReadLine();

                IMoziLogic ml = new MoziLogic();
                ml.CreateMozi(new Mozi { nev = name, varos = city, ferohely = max, vane3D = d, vanefelirat = sub });

                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("CREATION WAS SUCCESSFUL!");

                Console.WriteLine("---------------------------------------------------------------");
                Console.WriteLine($"{name} || {city} || {max} || 3D? {d} || Felirat? {sub}");
                Console.WriteLine("---------------------------------------------------------------");
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("CREATION FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
            }
        }

        /// <summary>
        /// Creates a programme object.
        /// </summary>
        private void CreateProgramme()
        {
            try
            {
                Console.Write("DATE: (YYY.MM.DD)");
                string date = Console.ReadLine();
                Console.Write("AUDIENCE: ");
                int aud = int.Parse(Console.ReadLine());
                Console.Write("INCOME: ");
                int income = int.Parse(Console.ReadLine());

                IMozimusorLogic ml = new MozimusorLogic();
                ml.CreateMozimusor(new Mozimusor { vetites_datum = DateTime.Parse(date), nezoszam = aud, bevetel = income });

                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("CREATION WAS SUCCESSFUL!");

                Console.WriteLine("---------------------------------------------------------------");
                Console.WriteLine($"{date} || {aud} || {income} HUF");
                Console.WriteLine("---------------------------------------------------------------");
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("CREATION FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Reads a specific food object.
        /// </summary>
        private void ReadFood()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.WriteLine();
                IBufeLogic bl = new BufeLogic();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("{0}: {1} HUF || {2} || {3} || allergének: {4}", bl.GetFoodByID(id).nev, bl.GetFoodByID(id).ar, bl.GetFoodByID(id).tipus, bl.GetFoodByID(id).homerseklet, bl.GetFoodByID(id).allergen);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("READING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Reads a specific film object.
        /// </summary>
        private void ReadFilm()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.WriteLine();
                IFilmLogic fl = new FilmLogic();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("{0} || {1} || {2} || {3} || {4}", fl.GetFilmByID(id).cim, fl.GetFilmByID(id).mufaj, fl.GetFilmByID(id).hossz, fl.GetFilmByID(id).ev, fl.GetFilmByID(id).foszereplo);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("READING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Reads a specific cinema object.
        /// </summary>
        private void ReadCinema()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.WriteLine();
                IMoziLogic ml = new MoziLogic();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("{0} || {1} || Férőhely: {2} || 3D? {3} || Felirat? {4}", ml.GetMoziByID(id).nev, ml.GetMoziByID(id).varos, ml.GetMoziByID(id).ferohely, ml.GetMoziByID(id).vane3D, ml.GetMoziByID(id).vanefelirat);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("READING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Reads a specific programme object.
        /// </summary>
        private void ReadProgramme()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.WriteLine();
                IMozimusorLogic ml = new MozimusorLogic();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("{0} || {1} || {2} HUF", ml.GetMozimusorByID(id).vetites_datum.ToShortDateString(), ml.GetMozimusorByID(id).nezoszam, ml.GetMozimusorByID(id).bevetel);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("READING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Lists all foods.
        /// </summary>
        private void ListFoods()
        {
            try
            {
                IBufeLogic bl = new BufeLogic();
                var foods = bl.GetFoods();
                foreach (var item in foods)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("{0} || {1} HUF || {2} || {3} || {4}", item.nev, item.ar, item.tipus, item.homerseklet, item.allergen);
                    Console.WriteLine("----------------------------------------------------------");
                    Console.WriteLine();
                    Console.ResetColor();
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("LISTING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Lists all films.
        /// </summary>
        private void ListFilms()
        {
            try
            {
                IFilmLogic fl = new FilmLogic();
                var films = fl.GetFilms();
                foreach (var item in films)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("{0} || {1} || {2} || {3} || {4}", item.cim, item.mufaj, item.hossz, item.ev, item.foszereplo);
                    Console.WriteLine("-----------------------------------------------------------------");
                    Console.ResetColor();
                    Console.WriteLine();
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("LISTING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Lists all cinemas.
        /// </summary>
        private void ListCinemas()
        {
            try
            {
                IMoziLogic ml = new MoziLogic();
                var cinemas = ml.GetMozis();
                foreach (var item in cinemas)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("{0} || {1} || {2} || 3D? {3} || Felirat? {4}", item.nev, item.varos, item.ferohely, item.vane3D, item.vanefelirat);
                    Console.WriteLine("----------------------------------------------------------");
                    Console.ResetColor();
                    Console.WriteLine();
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("LISTING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Lists all programmes.
        /// </summary>
        private void ListProgrammes()
        {
            try
            {
                IMozimusorLogic ml = new MozimusorLogic();
                var progs = ml.GetMozimusors();
                foreach (var item in progs)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("{0} || {1} || {2} ", item.vetites_datum.ToShortDateString(), item.nezoszam, item.bevetel);
                    Console.WriteLine("----------------------------------------------------------");
                    Console.ResetColor();
                    Console.WriteLine();
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("LISTING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Deletes a specific food object.
        /// </summary>
        private void DeleteFood()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                IBufeLogic bl = new BufeLogic();
                bl.DeleteFood(id);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY DELETED!");
                Console.ResetColor();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("DELETING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Deletes a specific film object.
        /// </summary>
        private void DeleteFilm()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                IFilmLogic fl = new FilmLogic();
                fl.DeleteFilm(id);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY DELETED!");
                Console.ResetColor();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("DELETING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Deletes a specific cinema object.
        /// </summary>
        private void DeleteCinema()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                IMoziLogic ml = new MoziLogic();
                ml.DeleteMozi(id);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY DELETED!");
                Console.ResetColor();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("DELETING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Deletes a specific programme object.
        /// </summary>
        private void DeleteProgramme()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                IMozimusorLogic ml = new MozimusorLogic();
                ml.DeleteMozimusor(id);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY DELETED!");
                Console.ResetColor();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("DELETING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Updates price.
        /// </summary>
        private void UpdatePrice()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.Write("ITEM'S NEW PRICE: ");
                int price = int.Parse(Console.ReadLine());
                IBufeLogic bl = new BufeLogic();
                bl.UpdateAr(id, price);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY UPDATED!");
                Console.WriteLine();
                Console.WriteLine("{0}: {1} HUF || {2} || {3} || allergének: {4}", bl.GetFoodByID(id).nev, bl.GetFoodByID(id).ar, bl.GetFoodByID(id).tipus, bl.GetFoodByID(id).homerseklet, bl.GetFoodByID(id).allergen);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("UPDATING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Updates allergens.
        /// </summary>
        private void UpdateAllerg()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.Write("ITEM'S NEW ALLERGEN NUMBER: ");
                int all = int.Parse(Console.ReadLine());
                IBufeLogic bl = new BufeLogic();
                bl.UpdateAllergen(id, all);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY UPDATED!");
                Console.WriteLine();
                Console.WriteLine("{0}: {1} HUF || {2} || {3} || allergének: {4}", bl.GetFoodByID(id).nev, bl.GetFoodByID(id).ar, bl.GetFoodByID(id).tipus, bl.GetFoodByID(id).homerseklet, bl.GetFoodByID(id).allergen);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("UPDATING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Updates temperature.
        /// </summary>
        private void UpdateTemp()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.Write("ITEM'S NEW TEMPERATURE (meleg/hideg): ");
                string temp = Console.ReadLine();
                IBufeLogic bl = new BufeLogic();
                bl.UpdateHomerseklet(id, temp);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY UPDATED!");
                Console.WriteLine();
                Console.WriteLine("{0}: {1} HUF || {2} || {3} || allergének: {4}", bl.GetFoodByID(id).nev, bl.GetFoodByID(id).ar, bl.GetFoodByID(id).tipus, bl.GetFoodByID(id).homerseklet, bl.GetFoodByID(id).allergen);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("UPDATING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Updates length.
        /// </summary>
        private void UpdateLength()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.Write("ITEM'S NEW LENGTH (mins): ");
                int length = int.Parse(Console.ReadLine());
                IFilmLogic fl = new FilmLogic();
                fl.UpdateHossz(id, length);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY UPDATED!");
                Console.WriteLine();
                Console.WriteLine("{0} || {1} || {2} || {3} || {4}", fl.GetFilmByID(id).cim, fl.GetFilmByID(id).mufaj, fl.GetFilmByID(id).ev, fl.GetFilmByID(id).hossz, fl.GetFilmByID(id).foszereplo);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("UPDATING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Updates year.
        /// </summary>
        private void UpdateYear()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.Write("ITEM'S NEW YEAR: ");
                int year = int.Parse(Console.ReadLine());
                IFilmLogic fl = new FilmLogic();
                fl.UpdateEv(id, year);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY UPDATED!");
                Console.WriteLine();
                Console.WriteLine("{0} || {1} || {2} || {3} || {4}", fl.GetFilmByID(id).cim, fl.GetFilmByID(id).mufaj, fl.GetFilmByID(id).ev, fl.GetFilmByID(id).hossz, fl.GetFilmByID(id).foszereplo);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("UPDATING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Updates actor.
        /// </summary>
        private void UpdateActor()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.Write("ITEM'S NEW ACTOR: ");
                string actor = Console.ReadLine();
                IFilmLogic fl = new FilmLogic();
                fl.UpdateFoszereplo(id, actor);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY UPDATED!");
                Console.WriteLine();
                Console.WriteLine("{0} || {1} || {2} || {3} || {4}", fl.GetFilmByID(id).cim, fl.GetFilmByID(id).mufaj, fl.GetFilmByID(id).ev, fl.GetFilmByID(id).hossz, fl.GetFilmByID(id).foszereplo);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("UPDATING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Updates 3D.
        /// </summary>
        private void Update3D()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.Write("3D: IGEN/NEM? ");
                string dontes = Console.ReadLine();
                IMoziLogic ml = new MoziLogic();
                ml.Update3D(id, dontes);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY UPDATED!");
                Console.WriteLine();
                Console.WriteLine("{0} || {1} || kapacitás: {2} || 3D? {3} || Felirat? {4}", ml.GetMoziByID(id).nev, ml.GetMoziByID(id).varos, ml.GetMoziByID(id).ferohely, ml.GetMoziByID(id).vane3D, ml.GetMoziByID(id).vanefelirat);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("UPDATING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Updates subtitles.
        /// </summary>
        private void UpdateSubs()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.Write("SUBS: IGEN/NEM? ");
                string dontes = Console.ReadLine();
                IMoziLogic ml = new MoziLogic();
                ml.UpdateFelirat(id, dontes);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY UPDATED!");
                Console.WriteLine();
                Console.WriteLine("{0} || {1} || kapacitás: {2} || 3D? {3} || Felirat? {4}", ml.GetMoziByID(id).nev, ml.GetMoziByID(id).varos, ml.GetMoziByID(id).ferohely, ml.GetMoziByID(id).vane3D, ml.GetMoziByID(id).vanefelirat);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("UPDATING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Updates the number of seats.
        /// </summary>
        private void UpdateSeats()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.Write("ITEM'S NEW NUMBER OF SEATS: ");
                int seats = int.Parse(Console.ReadLine());
                IMoziLogic ml = new MoziLogic();
                ml.UpdateFerohely(id, seats);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY UPDATED!");
                Console.WriteLine();
                Console.WriteLine("{0} || {1} || kapacitás: {2} || 3D? {3} || Felirat? {4}", ml.GetMoziByID(id).nev, ml.GetMoziByID(id).varos, ml.GetMoziByID(id).ferohely, ml.GetMoziByID(id).vane3D, ml.GetMoziByID(id).vanefelirat);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("UPDATING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Updates income.
        /// </summary>
        private void UpdateIncome()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.Write("ITEM'S NEW INCOME (HUF): ");
                int income = int.Parse(Console.ReadLine());
                IMozimusorLogic ml = new MozimusorLogic();
                ml.UpdateBevetel(id, income);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY UPDATED!");
                Console.WriteLine();
                Console.WriteLine("{0} || {1} ||  {2} HUF", ml.GetMozimusorByID(id).vetites_datum.ToShortDateString(), ml.GetMozimusorByID(id).nezoszam, ml.GetMozimusorByID(id).bevetel);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("UPDATING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Updates the number of audience.
        /// </summary>
        private void UpdateAudience()
        {
            try
            {
                Console.Write("ITEM'S ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.Write("ITEM'S NEW AUDIENCE: ");
                int aud = int.Parse(Console.ReadLine());
                IMozimusorLogic ml = new MozimusorLogic();
                ml.UpdateNezoszam(id, aud);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("ITEM SUCCESSFULLY UPDATED!");
                Console.WriteLine();
                Console.WriteLine("{0} || {1} ||  {2} HUF", ml.GetMozimusorByID(id).vetites_datum.ToShortDateString(), ml.GetMozimusorByID(id).nezoszam, ml.GetMozimusorByID(id).bevetel);
                Console.ResetColor();
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("UPDATING FAILED, TRY AGAIN.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Returns the selection's average price.
        /// </summary>
        private void AvgPrice()
        {
            Console.WriteLine("HERE IS OUR FIVE STAR SELECTION'S AVERAGE PRICE:");
            Console.ForegroundColor = ConsoleColor.Blue;
            IBufeLogic bl = new BufeLogic();
            Console.WriteLine("\n" + bl.AvgPrice() + " HUF");
            Console.ResetColor();
            Console.WriteLine();
        }

        /// <summary>
        /// Returns the newest movie's title in the database.
        /// </summary>
        private void NewestMovie()
        {
            Console.WriteLine("YOU CAN SEE THE NEWEST MOVIE BELOW:");
            Console.ForegroundColor = ConsoleColor.Blue;
            IFilmLogic fl = new FilmLogic();
            var film = fl.GetTheNewestFilm();
            foreach (var item in film)
            {
                Console.WriteLine("\n" + item + " (2019)");
            }

            Console.WriteLine();
            Console.WriteLine("WATCH THE TRAILER HERE: \nhttps://www.youtube.com/watch?v=-_DJEzZk2pc");

            Console.ResetColor();
            Console.WriteLine();
        }

        /// <summary>
        /// Gets the top3 cinemas with more than 200 seats.
        /// </summary>
        private void Top3Cinemas()
        {
            Console.WriteLine("YOU CAN FIND PLENTY OF OUR CINEMAS WITH MORE THAN 200 SEATS - IN THE WHOLE COUNTRY!");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine();
            IMoziLogic ml = new MoziLogic();
            var mozik = ml.CinemasWithMostSeats();
            int i = 0;
            foreach (var item in mozik)
            {
                i++;
                Console.WriteLine(i + ".) " + item);
            }

            Console.ResetColor();
            Console.WriteLine();
        }

        /// <summary>
        /// Gets the number of visitors.
        /// </summary>
        private void AudienceCounter()
        {
            Console.WriteLine("HERE'S THE CURRENT NUMBER OF OUR VISITORS SINCE WE OPENED - AND IT'S COUNTING!");
            Console.WriteLine();
            IMozimusorLogic ml = new MozimusorLogic();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(ml.AudienceCounter());
            Console.WriteLine();
            Console.ResetColor();
        }

        /// <summary>
        /// Java connection.
        /// </summary>
        private void JavaConnection()
        {
            Console.Clear();
            string url = $"http://localhost:8080/OENIK_PROG3_2019_2_C0T9T9/servlet";
            XDocument xDoc = XDocument.Load(url);

            Console.WriteLine("-- IF YOU HAVE NO IDEA WHAT TO WATCH! --");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Blue;
            foreach (var film in xDoc.Descendants("film"))
            {
                Console.WriteLine("{0}", film.Element("napi").Value);
            }

            Console.WriteLine();
            Console.ResetColor();
        }
    }
}
