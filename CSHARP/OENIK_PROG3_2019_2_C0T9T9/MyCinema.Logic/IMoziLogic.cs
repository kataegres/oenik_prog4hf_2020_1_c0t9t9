﻿// <copyright file="IMoziLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Logic
{
    using System.Collections.Generic;
    using MyCinema.Data;

    /// <summary>
    /// IMoziLogic interface.
    /// </summary>
    public interface IMoziLogic
    {
        /// <summary>
        /// Gets the top 3 cinemas with more than 200 seats.
        /// </summary>
        /// <returns>A string array.</returns>
        string[] CinemasWithMostSeats();

        /// <summary>
        /// Creates a new cinema.
        /// </summary>
        /// <param name="newmozi">A new cinema object.</param>
        void CreateMozi(Mozi newmozi);

        /// <summary>
        /// Deletes a cinema.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        void DeleteMozi(int id);

        /// <summary>
        /// Gets a cinema by ID.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A cinema object.</returns>
        Mozi GetMoziByID(int id);

        /// <summary>
        /// Gets all cinemas.
        /// </summary>
        /// <returns>A list.</returns>
        List<Mozi> GetMozis();

        /// <summary>
        /// Updates 3D.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="uj">Yes/no.</param>
        void Update3D(int id, string uj);

        /// <summary>
        /// Updates subs.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="uj">Yes/no.</param>
        void UpdateFelirat(int id, string uj);

        /// <summary>
        /// Updates seats.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujferohely">Item's new number of seats.</param>
        void UpdateFerohely(int id, int ujferohely);
    }
}