﻿// <copyright file="FilmLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCinema.Data;
    using MyCinema.Repository;

    /// <summary>
    /// Film logic class.
    /// </summary>
    public class FilmLogic : IFilmLogic
    {
        private readonly IFilmRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FilmLogic"/> class.
        /// Constructor.
        /// </summary>
        public FilmLogic()
        {
            this.repo = new FilmRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FilmLogic"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="repo">A Film repository interface.</param>
        public FilmLogic(IFilmRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Adds a new film to the table.
        /// </summary>
        /// <param name="newfilm">A film object.</param>
        public void CreateFilm(Film newfilm)
        {
            newfilm.filmID = this.repo.LastID() + 1;
            this.repo.AddFilm(newfilm);
        }

        /// <summary>
        /// Deletes a specific film.
        /// </summary>
        /// <param name="id">Id of film.</param>
        /// <returns>A boolean.</returns>
        public bool DeleteFilm(int id)
        {
            return this.repo.DeleteFilm(id);
        }

        /// <summary>
        /// Updates the length of a film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujhossz">Item's new length.</param>
        public void UpdateHossz(int id, int ujhossz)
        {
            this.repo.UpdateHossz(id, ujhossz);
        }

        /// <summary>
        /// Updates the release year of a film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujev">Item's new release year.</param>
        public void UpdateEv(int id, int ujev)
        {
            this.repo.UpdateEv(id, ujev);
        }

        /// <summary>
        /// Updates the main actor/actress of a film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujfoszereplo">Item's new main actor/actress.</param>
        public void UpdateFoszereplo(int id, string ujfoszereplo)
        {
            this.repo.UpdateFoszereplo(id, ujfoszereplo);
        }

        /// <summary>
        /// Gets a specific film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A film object.</returns>
        public Film GetFilmByID(int id)
        {
            return this.repo.GetFilmByID(id);
        }

        /// <summary>
        /// Gets a T generic type collection.
        /// </summary>
        /// <returns>A list.</returns>
        public List<Film> GetFilms()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Gets the newest film in the database.
        /// </summary>
        /// <returns>A string array.</returns>
        public string[] GetTheNewestFilm()
        {
            var res = this.GetFilms().OrderByDescending(t => t.ev).ToArray();
            return new string[]
            {
                res[0].cim,
            };
        }

        /// <summary>
        /// Changes a film object's data.
        /// </summary>
        /// <param name="filmId">Id of film.</param>
        /// <param name="cim">Title of film.</param>
        /// <param name="mufaj">Genre of film.</param>
        /// <param name="hossz">Length of film.</param>
        /// <param name="ev">Year of film.</param>
        /// <param name="foszereplo">Main actor of film.</param>
        /// <returns>A boolean.</returns>
        public bool ChangeFilm(int filmId, string cim, string mufaj, int hossz, int ev, string foszereplo)
        {
            return this.repo.ChangeFilm(filmId, cim, mufaj, hossz, ev, foszereplo);
        }
    }
}
