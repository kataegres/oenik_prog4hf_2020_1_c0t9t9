﻿// <copyright file="BufeLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCinema.Data;
    using MyCinema.Repository;

    /// <summary>
    /// Bufe logic class.
    /// </summary>
    public class BufeLogic : IBufeLogic
    {
        private readonly IBufeRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="BufeLogic"/> class.
        /// Constructor.
        /// </summary>
        public BufeLogic()
        {
            this.repo = new BufeRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BufeLogic"/> class.
        /// Ctor.
        /// </summary>
        /// <param name="repo">A Bufe repository interface.</param>
        public BufeLogic(IBufeRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Adds a new food object to the table.
        /// </summary>
        /// <param name="newfood">A food object.</param>
        public void CreateFood(Bufe newfood)
        {
            if (newfood.ar <= 0)
            {
                throw new ArgumentException("Price must be positive!");
            }

            newfood.termekID = this.repo.LastID() + 1;
            this.repo.AddFood(newfood);
        }

        /// <summary>
        /// Deletes a specific food.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        public void DeleteFood(int id)
        {
            this.repo.DeleteFood(id);
        }

        /// <summary>
        /// Updates the price of a food.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ar">Item's new price.</param>
        public void UpdateAr(int id, int ar)
        {
            this.repo.UpdateAr(id, ar);
        }

        /// <summary>
        /// Updates the number of allergens of a food.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="allergen">Item's new number of allergens.</param>
        public void UpdateAllergen(int id, int allergen)
        {
            this.repo.UpdateAllergen(id, allergen);
        }

        /// <summary>
        /// Updates the temperature of a food.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="homerseklet">Item's new temperature.</param>
        public void UpdateHomerseklet(int id, string homerseklet)
        {
            this.repo.UpdateHomerseklet(id, homerseklet);
        }

        /// <summary>
        /// Gets a specific food object.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A food object.</returns>
        public Bufe GetFoodByID(int id)
        {
            return this.repo.GetFoodByID(id);
        }

        /// <summary>
        /// Gets a T generic type collection.
        /// </summary>
        /// <returns>A list.</returns>
        public List<Bufe> GetFoods()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Returns the selection's average price.
        /// </summary>
        /// <returns>An integer.</returns>
        public int AvgPrice()
        {
            return (int)this.repo.GetAll().Average(x => x.ar);
        }
    }
}
