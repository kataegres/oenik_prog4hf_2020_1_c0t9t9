﻿// <copyright file="IMozimusorLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Logic
{
    using System.Collections.Generic;
    using MyCinema.Data;

    /// <summary>
    /// IMozimusorLogic interface.
    /// </summary>
    public interface IMozimusorLogic
    {
        /// <summary>
        /// Gets the current count of the visitors.
        /// </summary>
        /// <returns>An integer.</returns>
        int AudienceCounter();

        /// <summary>
        /// Creates a progeamme.
        /// </summary>
        /// <param name="newmusor">A new programme object.</param>
        void CreateMozimusor(Mozimusor newmusor);

        /// <summary>
        /// Deletes a programme.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        void DeleteMozimusor(int id);

        /// <summary>
        /// Gets a programme by ID.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A programme object.</returns>
        Mozimusor GetMozimusorByID(int id);

        /// <summary>
        /// Gets all programmes.
        /// </summary>
        /// <returns>A list.</returns>
        List<Mozimusor> GetMozimusors();

        /// <summary>
        /// Updates the income.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujbevetel">Item's new income.</param>
        void UpdateBevetel(int id, int ujbevetel);

        /// <summary>
        /// Updates the number of audience.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujnezoszam">Item's new number of audience.</param>
        void UpdateNezoszam(int id, int ujnezoszam);
    }
}