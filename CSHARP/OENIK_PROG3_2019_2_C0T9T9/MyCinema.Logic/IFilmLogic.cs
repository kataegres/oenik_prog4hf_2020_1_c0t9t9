﻿// <copyright file="IFilmLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Logic
{
    using System.Collections.Generic;
    using MyCinema.Data;

    /// <summary>
    /// IFilmLogic interface.
    /// </summary>
    public interface IFilmLogic
    {
        /// <summary>
        /// Creates a film object.
        /// </summary>
        /// <param name="newfilm">A film object.</param>
        void CreateFilm(Film newfilm);

        /// <summary>
        /// Deletes a film object.
        /// </summary>
        /// <param name="id">Id of film.</param>
        /// <returns>A boolean.</returns>
        bool DeleteFilm(int id);

        /// <summary>
        /// Gets a film by ID.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A film object.</returns>
        Film GetFilmByID(int id);

        /// <summary>
        /// Gets all films.
        /// </summary>
        /// <returns>A list.</returns>
        List<Film> GetFilms();

        /// <summary>
        /// Gets the newest film in the database.
        /// </summary>
        /// <returns>A string array.</returns>
        string[] GetTheNewestFilm();

        /// <summary>
        /// Updates the year of a film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujev">Item's new year.</param>
        void UpdateEv(int id, int ujev);

        /// <summary>
        /// Updates the main actor of a film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujfoszereplo">Item's new actor.</param>
        void UpdateFoszereplo(int id, string ujfoszereplo);

        /// <summary>
        /// Updates the length of a film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujhossz">Item's new length.</param>
        void UpdateHossz(int id, int ujhossz);

        /// <summary>
        /// Changes a film object's data.
        /// </summary>
        /// <param name="filmId">Id of film.</param>
        /// <param name="cim">Title of film.</param>
        /// <param name="mufaj">Genre of film.</param>
        /// <param name="hossz">Length of film.</param>
        /// <param name="ev">Year of film.</param>
        /// <param name="foszereplo">Main actor of film.</param>
        /// <returns>A boolean.</returns>
        bool ChangeFilm(int filmId, string cim, string mufaj, int hossz, int ev, string foszereplo);
    }
}