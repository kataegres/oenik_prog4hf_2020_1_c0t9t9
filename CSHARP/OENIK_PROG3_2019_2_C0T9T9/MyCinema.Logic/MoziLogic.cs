﻿// <copyright file="MoziLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCinema.Data;
    using MyCinema.Repository;

    /// <summary>
    /// Mozi logic class.
    /// </summary>
    public class MoziLogic : IMoziLogic
    {
        private readonly IMoziRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="MoziLogic"/> class.
        /// Constructor.
        /// </summary>
        public MoziLogic()
        {
            this.repo = new MoziRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MoziLogic"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="repo">A Mozi repository interface.</param>
        public MoziLogic(IMoziRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Adds a new cinema to the table.
        /// </summary>
        /// <param name="newmozi">A new cinema object.</param>
        public void CreateMozi(Mozi newmozi)
        {
            newmozi.moziID = this.repo.LastID() + 1;
            this.repo.AddMozi(newmozi);
        }

        /// <summary>
        /// Deletes a specific cinema.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        public void DeleteMozi(int id)
        {
            this.repo.DeleteMozi(id);
        }

        /// <summary>
        /// Updates if the cinema plays 3D films (yes/no).
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="uj">Yes/no.</param>
        public void Update3D(int id, string uj)
        {
            this.repo.Update3D(id, uj);
        }

        /// <summary>
        /// Updates if the cinema plays subtitled films (yes/no).
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="uj">Yes/no.</param>
        public void UpdateFelirat(int id, string uj)
        {
            this.repo.UpdateFelirat(id, uj);
        }

        /// <summary>
        /// Updates the number of vacant seats.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujferohely">Item's new number of seats.</param>
        public void UpdateFerohely(int id, int ujferohely)
        {
            this.repo.UpdateFerohely(id, ujferohely);
        }

        /// <summary>
        /// Gets a specific cinema.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A cinema object.</returns>
        public Mozi GetMoziByID(int id)
        {
            return this.repo.GetMoziByID(id);
        }

        /// <summary>
        /// Gets a T generic type collection.
        /// </summary>
        /// <returns>A list.</returns>
        public List<Mozi> GetMozis()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Gets the top 3 cinemas with more than 200 seats.
        /// </summary>
        /// <returns>A string array.</returns>
        public string[] CinemasWithMostSeats()
        {
            var ordered = this.repo.GetAll()
                .OrderByDescending(t => t.ferohely)
                .Where(t => t.ferohely > 200)
                .Select(t => t.nev);

            return ordered.Take(3).ToArray();
        }
    }
}
