﻿// <copyright file="MozimusorLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCinema.Data;
    using MyCinema.Repository;

    /// <summary>
    /// Mozimusor logic class.
    /// </summary>
    public class MozimusorLogic : IMozimusorLogic
    {
        private readonly IMozimusorRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="MozimusorLogic"/> class.
        /// Constructor.
        /// </summary>
        public MozimusorLogic()
        {
            this.repo = new MozimusorRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MozimusorLogic"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="repo">A Mozimusor repository interface.</param>
        public MozimusorLogic(IMozimusorRepository repo)
        {
            this.repo = repo;
        }

        /// <summary>
        /// Adds a new programme to the table.
        /// </summary>
        /// <param name="newmusor">A new programme object.</param>
        public void CreateMozimusor(Mozimusor newmusor)
        {
            newmusor.mozimusorID = this.repo.LastID() + 1;
            this.repo.AddMozimusor(newmusor);
        }

        /// <summary>
        /// Deletes a specific programme.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        public void DeleteMozimusor(int id)
        {
            this.repo.DeleteMusor(id);
        }

        /// <summary>
        /// Updates the income of a specific programme.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujbevetel">Item's new income.</param>
        public void UpdateBevetel(int id, int ujbevetel)
        {
            this.repo.UpdateBevetel(id, ujbevetel);
        }

        /// <summary>
        /// Updates the number of audience of a programme.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujnezoszam">Item's new audience.</param>
        public void UpdateNezoszam(int id, int ujnezoszam)
        {
            this.repo.UpdateNezoszam(id, ujnezoszam);
        }

        /// <summary>
        /// Gets a specific programme.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A programme object.</returns>
        public Mozimusor GetMozimusorByID(int id)
        {
            return this.repo.GetMozimusorByID(id);
        }

        /// <summary>
        /// Gets a T generic type collection.
        /// </summary>
        /// <returns>A list.</returns>
        public List<Mozimusor> GetMozimusors()
        {
            return this.repo.GetAll().ToList();
        }

        /// <summary>
        /// Gets the current number of the visitors since the opening.
        /// </summary>
        /// <returns>An integer.</returns>
        public int AudienceCounter()
        {
            return (int)this.GetMozimusors().Sum(t => t.nezoszam);
        }
    }
}
