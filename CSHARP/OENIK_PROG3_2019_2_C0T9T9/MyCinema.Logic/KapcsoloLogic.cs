﻿// <copyright file="KapcsoloLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCinema.Data;
    using MyCinema.Repository;

    /// <summary>
    /// .
    /// </summary>
    public class KapcsoloLogic
    {
        private readonly KapcsoloRepository repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="KapcsoloLogic"/> class.
        /// .
        /// </summary>
        public KapcsoloLogic()
        {
            this.repo = new KapcsoloRepository();
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        public void DeleteConnectionByFilmID(int id)
        {
            this.repo.DeleteConnectionByFilmID(id);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        public void DeleteConnectionByMoziID(int id)
        {
            this.repo.DeleteConnectionByMoziID(id);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        public void DeleteConnectionByMozimusorID(int id)
        {
            this.repo.DeleteConnectionByMozimusorID(id);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <returns>A list.</returns>
        public IQueryable<Kapcsolo> GetAll()
        {
            return this.repo.GetAll();
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A connection object.</returns>
        public Kapcsolo GetConnectionByFilmID(int id)
        {
            return this.repo.GetConnectionByFilmID(id);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// /// <returns>A connection object.</returns>
        public Kapcsolo GetConnectionByMoziID(int id)
        {
            return this.repo.GetConnectionByMoziID(id);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// /// <returns>A connection object.</returns>
        public Kapcsolo GetConnectionByMozimusorID(int id)
        {
            return this.repo.GetConnectionByMozimusorID(id);
        }
    }
}
