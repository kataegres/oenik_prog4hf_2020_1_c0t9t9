﻿// <copyright file="IBufeLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Logic
{
    using System.Collections.Generic;
    using MyCinema.Data;

    /// <summary>
    /// IBufeLogic interface.
    /// </summary>
    public interface IBufeLogic
    {
        /// <summary>
        /// Creates a food object.
        /// </summary>
        /// <param name="newfood">A food object.</param>
        void CreateFood(Bufe newfood);

        /// <summary>
        /// Deletes a food object.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        void DeleteFood(int id);

        /// <summary>
        /// Gets a food by ID.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A food object.</returns>
        Bufe GetFoodByID(int id);

        /// <summary>
        /// Gets all food objects.
        /// </summary>
        /// <returns>A list.</returns>
        List<Bufe> GetFoods();

        /// <summary>
        /// Updates the number of allergens.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="allergen">Item's allergens.</param>
        void UpdateAllergen(int id, int allergen);

        /// <summary>
        /// Updates the price.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ar">Item's price.</param>
        void UpdateAr(int id, int ar);

        /// <summary>
        /// Updates temperature.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="homerseklet">Item's temperature.</param>
        void UpdateHomerseklet(int id, string homerseklet);

        /// <summary>
        /// Returns the selection's average price.
        /// </summary>
        /// <returns>An integer.</returns>
        int AvgPrice();
    }
}