﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCinema.Wpf
{
    class FilmVM : ObservableObject
    {
        int id;
        string cim;
        string mufaj;
        int hossz;
        int ev;
        string foszereplo;

        public int Id
        {
            get { return id; }
            set { Set(ref id, value); }
        }

        public string Cim
        {
            get { return cim; }
            set { Set(ref cim, value); }
        }

        public string Mufaj
        {
            get { return mufaj; }
            set { Set(ref mufaj, value); }
        }

        public int Hossz
        {
            get { return hossz; }
            set { Set(ref hossz, value); }
        }

        public int Ev
        {
            get { return ev; }
            set { Set(ref ev, value); }
        }

        public string Foszereplo
        {
            get { return foszereplo; }
            set { Set(ref foszereplo, value); }
        }

        public void CopyFrom(FilmVM other)
        {
            if (other == null)
            {
                return;
            }
            this.Id = other.Id;
            this.Cim = other.Cim;
            this.Mufaj = other.Mufaj;
            this.Hossz = other.Hossz;
            this.Ev = other.Ev;
            this.Foszereplo = other.Foszereplo;
        }
    }
}
