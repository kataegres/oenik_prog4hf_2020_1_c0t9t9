﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MyCinema.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private ObservableCollection<FilmVM> allFilms;
        private FilmVM selectedFilm;

        public FilmVM SelectedFilm
        {
            get { return selectedFilm; }
            set { Set(ref selectedFilm, value); }
        }
        public ObservableCollection<FilmVM> AllFilms
        {
            get { return allFilms; }
            set { Set(ref allFilms, value); }
        }

        public ICommand AddCmd { get; private set; }
        public ICommand DelCmd { get; private set; }
        public ICommand ModCmd { get; private set; }
        public ICommand LoadCmd { get; private set; }

        public Func<FilmVM, bool> EditorFunc { get; set; }

        public MainVM()
        {
            logic = new MainLogic();
            DelCmd = new RelayCommand(() => logic.ApiDelFilm(selectedFilm));
            AddCmd = new RelayCommand(() => logic.EditFilm(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.EditFilm(selectedFilm, EditorFunc));
            LoadCmd = new RelayCommand(() => AllFilms = new ObservableCollection<FilmVM>(logic.ApiGetFilms()));

        }
    }
}
