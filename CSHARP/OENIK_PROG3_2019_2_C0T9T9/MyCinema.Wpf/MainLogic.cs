﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyCinema.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:52568/api/FilmApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "FilmResult");
        }

        public List<FilmVM> ApiGetFilms()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<FilmVM>>(json);
            //SendMessage(true);
            return list;
        }

        public void ApiDelFilm(FilmVM film)
        {
            bool success = false;
            if (film != null)
            {
                string json = client.GetStringAsync(url + "del/" + film.Id.ToString()).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditFilm(FilmVM film, bool isEditing)
        {
            if (film == null)
            {
                return false;
            }
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add(nameof(FilmVM.Id), film.Id.ToString());
            }
            postData.Add(nameof(FilmVM.Cim), film.Cim);
            postData.Add(nameof(FilmVM.Mufaj), film.Mufaj);
            postData.Add(nameof(FilmVM.Hossz), film.Hossz.ToString());
            postData.Add(nameof(FilmVM.Ev), film.Ev.ToString());
            postData.Add(nameof(FilmVM.Foszereplo), film.Foszereplo);

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];

        }

        public void EditFilm(FilmVM film, Func<FilmVM, bool> editor)
        {
            FilmVM clone = new FilmVM();
            if (film != null)
            {
                clone.CopyFrom(film);
            }
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (film != null)
                {
                    success = ApiEditFilm(clone, true);
                }
                else
                {
                    success = ApiEditFilm(clone, false);
                }
            }
            SendMessage(success == true);

        }
    }
}
