﻿// <copyright file="CinemaTester.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using MyCinema.Data;
    using MyCinema.Logic;
    using MyCinema.Repository;
    using NUnit;
    using NUnit.Framework;

    /// <summary>
    /// Test class.
    /// </summary>
    [TestFixture]
    public class CinemaTester
    {
        private Mock<IBufeRepository> mockedRepoBufe;
        private BufeLogic bl;

        private Mock<IFilmRepository> mockedRepoFilm;
        private FilmLogic fl;

        private Mock<IMoziRepository> mockedRepoMozi;
        private MoziLogic ml;

        private Mock<IMozimusorRepository> mockedRepoMusor;
        private MozimusorLogic mrl;

        /// <summary>
        /// Initialization method.
        /// </summary>
        [SetUp]
        public void Init()
        {
            this.mockedRepoBufe = new Mock<IBufeRepository>();
            List<Bufe> termekek = new List<Bufe>()
            {
                new Bufe() { termekID = 1, nev = "Isler", ar = 350, tipus = "étel", allergen = 3, homerseklet = "hideg" },
                new Bufe() { termekID = 2, nev = "Kávé", ar = 150, tipus = "ital", allergen = 2, homerseklet = "meleg" },
                new Bufe() { termekID = 3, nev = "Perec", ar = 200, tipus = "étel", allergen = 4, homerseklet = "hideg" },
            };

            this.mockedRepoBufe.Setup(m => m.GetAll()).Returns(termekek.AsQueryable);
            this.mockedRepoBufe.Setup(m => m.GetFoodByID(It.IsAny<int>())).Returns((int i) => termekek.Where(t => t.termekID == i).Single());
            this.bl = new BufeLogic(this.mockedRepoBufe.Object);

            /*###########################################################################################################################*/

            this.mockedRepoFilm = new Mock<IFilmRepository>();

            List<Film> filmek = new List<Film>()
            {
                new Film() { filmID = 1, cim = "A király", hossz = 120, mufaj = "történelmi", ev = 2019, foszereplo = "Timothée Chalamet" },
                new Film() { filmID = 2, cim = "John Wick", hossz = 131, mufaj = "akció", ev = 2019, foszereplo = "Keanu Reeves" },
                new Film() { filmID = 3, cim = "A 64-es betegnapló", hossz = 104, mufaj = "thriller", ev = 2019, foszereplo = "Fares Fares" },
            };

            this.mockedRepoFilm.Setup(m => m.GetAll()).Returns(filmek.AsQueryable);
            this.mockedRepoFilm.Setup(m => m.GetFilmByID(It.IsAny<int>())).Returns((int i) => filmek.Where(t => t.filmID == i).Single());

            this.fl = new FilmLogic(this.mockedRepoFilm.Object);

            /*###########################################################################################################################*/

            this.mockedRepoMozi = new Mock<IMoziRepository>();

            List<Mozi> mozik = new List<Mozi>()
            {
                new Mozi() { moziID = 1, nev = "Csillag Mozi", varos = "Salgótarján", ferohely = 260, vane3D = "igen", vanefelirat = "nem" },
                new Mozi() { moziID = 2, nev = "Balaton Mozi", varos = "Keszthely", ferohely = 245, vane3D = "igen", vanefelirat = "igen" },
                new Mozi() { moziID = 3, nev = "Héra Mozi", varos = "Debrecen", ferohely = 210, vane3D = "igen", vanefelirat = "igen" },
                new Mozi() { moziID = 4, nev = "Duna Mozi", varos = "Budapest", ferohely = 120, vane3D = "nem", vanefelirat = "igen" },
            };

            this.mockedRepoMozi.Setup(m => m.GetAll()).Returns(mozik.AsQueryable);
            this.mockedRepoMozi.Setup(m => m.GetMoziByID(It.IsAny<int>())).Returns((int i) => mozik.Where(t => t.moziID == i).Single());
            this.ml = new MoziLogic(this.mockedRepoMozi.Object);

            /*###########################################################################################################################*/

            this.mockedRepoMusor = new Mock<IMozimusorRepository>();

            List<Mozimusor> musorok = new List<Mozimusor>()
            {
                new Mozimusor() { mozimusorID = 1, vetites_datum = DateTime.Parse("2019.11.05."), nezoszam = 35, bevetel = 87500 },
                new Mozimusor() { mozimusorID = 2, vetites_datum = DateTime.Parse("2019.11.06."), nezoszam = 76, bevetel = 190000 },
                new Mozimusor() { mozimusorID = 3, vetites_datum = DateTime.Parse("2019.11.07."), nezoszam = 53, bevetel = 132500 },
            };

            this.mockedRepoMusor.Setup(m => m.GetAll()).Returns(musorok.AsQueryable);
            this.mockedRepoMusor.Setup(m => m.GetMozimusorByID(It.IsAny<int>())).Returns((int i) => musorok.Where(t => t.mozimusorID == i).Single());
            this.mrl = new MozimusorLogic(this.mockedRepoMusor.Object);
        }

        /// <summary>
        /// Food creation test no. 1.
        /// </summary>
        [Test]
        public void CreateFood_Test() // Create
        {
            this.bl.CreateFood(new Bufe() { termekID = 2, nev = "Kávé", ar = 150, tipus = "ital", allergen = 2, homerseklet = "meleg" });
            this.mockedRepoBufe.Verify(t => t.AddFood(It.IsAny<Bufe>()), Times.Once);
        }

        /// <summary>
        /// Food creation test no. 2.
        /// </summary>
        [Test]
        public void CreateFood_Test2() // Create2
        {
            Assert.That(
                () => this.bl.CreateFood(new Bufe()
                {
                    termekID = 4,
                    nev = "Chips",
                    ar = -400,
                    allergen = 5,
                    tipus = "étel",
                    homerseklet = "hideg",
                }), Throws.TypeOf<ArgumentException>());
        }

        /// <summary>
        /// List all films test.
        /// </summary>
        [Test]
        public void ListFilms_Test() // ReadAll
        {
            List<Film> filmek = this.fl.GetFilms();
            Assert.That(filmek.Count > 0);
        }

        /// <summary>
        /// Get one film test.
        /// </summary>
        [Test]
        public void GetOneFilm_Test() // Read
        {
            Film egyfilm = this.fl.GetFilmByID(2);

            Assert.IsNotNull(egyfilm);
            Assert.IsInstanceOf(typeof(Film), egyfilm);
            Assert.AreEqual(2, egyfilm.filmID);
        }

        /// <summary>
        /// Update seats test..
        /// </summary>
        [Test]
        public void UpdateSeats_Test() // Update
        {
            this.ml.UpdateFerohely(4, 200);
            this.mockedRepoMozi.Verify(t => t.UpdateFerohely(4, 200), Times.Once);
        }

        /// <summary>
        /// Delete programme test.
        /// </summary>
        [Test]
        public void DeleteProgramme_Test() // Delete
        {
            this.mrl.DeleteMozimusor(1);
            this.mockedRepoMusor.Verify(t => t.DeleteMusor(1), Times.Once);
        }

        /// <summary>
        /// Average price test.
        /// </summary>
        [Test]
        public void AvgPrice_Test() // NON-CRUD1
        {
            decimal avg = this.bl.AvgPrice();
            Assert.That(avg > 0);
            Assert.That(avg < 1000);
        }

        /// <summary>
        /// Newest film test..
        /// </summary>
        [Test]
        public void NewestFilm_Test() // NON-CRUD2
        {
            string[] film = this.fl.GetTheNewestFilm();
            Assert.That(film[0].Length > 0);
        }

        /// <summary>
        /// Top3 cinemas test.
        /// </summary>
        [Test]
        public void Top3Cinemas_Test() // NON-CRUD3
        {
            var mozik = this.ml.CinemasWithMostSeats();
            Assert.That(mozik.Length == 3);
            Assert.That(mozik.Contains("Héra Mozi"));
        }

        /// <summary>
        /// Visitor counter test.
        /// </summary>
        [Test]
        public void VisitorCounter_Test() // NON-CRUD4
        {
            var sum = this.mrl.AudienceCounter();
            Assert.That(sum > this.mrl.GetMozimusorByID(1).nezoszam);
        }
    }
}
