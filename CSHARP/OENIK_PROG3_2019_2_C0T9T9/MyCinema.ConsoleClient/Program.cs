﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyCinema.ConsoleClient
{
    public class Film
    {
        public int Id { get; set; }
        public string Cim { get; set; }
        public string Mufaj { get; set; }
        public int Hossz { get; set; }
        public int Ev { get; set; }
        public string Foszereplo { get; set; }
        public override string ToString()
        {
            return $"ID={Id}\tTitle={Cim}\tGenre={Mufaj}\tLength={Hossz}\tRelease year={Ev}\tMain actor={Foszereplo}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:52568/api/FilmApi/";
            Console.WriteLine("WAITING...");
            Console.ReadLine();

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Film>>(json);
                foreach (var item in list) Console.WriteLine(item);
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Film.Cim), "Tőrbe ejtve");
                postData.Add(nameof(Film.Mufaj), "krimi");
                postData.Add(nameof(Film.Hossz), "132");
                postData.Add(nameof(Film.Ev), "2019");
                postData.Add(nameof(Film.Foszereplo), "Daniel Craig");


                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int filmid = JsonConvert.DeserializeObject<List<Film>>(json).Single(x => x.Cim == "Tőrbe ejtve").Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Film.Id), filmid.ToString());
                postData.Add(nameof(Film.Cim), "Tőrbe ejtve");
                postData.Add(nameof(Film.Mufaj), "krimi");
                postData.Add(nameof(Film.Hossz), "145");
                postData.Add(nameof(Film.Ev), "2019");
                postData.Add(nameof(Film.Foszereplo), "Daniel Craig");

                response = client.PostAsync(url + "mod",
                    new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "all").Result);
                Console.ReadLine();

                Console.WriteLine("DEL: " + client.GetStringAsync(url + "del/" + filmid).Result);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "all").Result);
                Console.ReadLine();
            }
        }
    }
}
