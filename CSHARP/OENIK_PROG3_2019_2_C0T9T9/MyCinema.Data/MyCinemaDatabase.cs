﻿// <copyright file="MyCinemaDatabase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Cinema database class.
    /// </summary>
    public static class MyCinemaDatabase
    {
        /// <summary>
        /// Initializes static members of the <see cref="MyCinemaDatabase"/> class.
        /// Constructor.
        /// </summary>
        static MyCinemaDatabase()
        {
            CinemaDB = new MyCinemaDatabaseEntities();
        }

        /// <summary>
        /// Gets cinema database object.
        /// </summary>
        public static MyCinemaDatabaseEntities CinemaDB { get; }
    }
}
