﻿// <copyright file="FilmRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCinema.Data;

    /// <summary>
    /// Film repository class.
    /// </summary>
    public class FilmRepository : IFilmRepository
    {
        private MyCinemaDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="FilmRepository"/> class.
        /// Constructor.
        /// </summary>
        public FilmRepository()
        {
            this.db = MyCinemaDatabase.CinemaDB;
        }

        /// <summary>
        /// Gets a T generic type collection.
        /// </summary>
        /// <returns>A list.</returns>
        public IQueryable<Film> GetAll()
        {
            return this.db.Films;
        }

        // CREATE

        /// <summary>
        /// Adds a new film item to the table.
        /// </summary>
        /// <param name="newfilm">A film object.</param>
        public void AddFilm(Film newfilm)
        {
            this.db.Films.Add(newfilm);
            this.db.SaveChanges();
        }

        // READ

        /// <summary>
        /// Gets a specific film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A film object.</returns>
        public Film GetFilmByID(int id)
        {
            return this.db.Films.FirstOrDefault(t => t.filmID == id);
        }

        // UPDATE

        /// <summary>
        /// Updates the length of a film (min).
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujhossz">Item's new length.</param>
        public void UpdateHossz(int id, int ujhossz)
        {
            var film = this.GetFilmByID(id);
            film.hossz = ujhossz;
            this.db.SaveChanges();
        }

        // UPDATE

        /// <summary>
        /// Updates the release year of a film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujev">Item's new release year.</param>
        public void UpdateEv(int id, int ujev)
        {
            var film = this.GetFilmByID(id);
            film.ev = ujev;
            this.db.SaveChanges();
        }

        // UPDATE

        /// <summary>
        /// Updates the main actor/actress of a film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujfoszereplo">Item's new actor/actress.</param>
        public void UpdateFoszereplo(int id, string ujfoszereplo)
        {
            var film = this.GetFilmByID(id);
            film.foszereplo = ujfoszereplo;
            this.db.SaveChanges();
        }

        // DELETE

        /// <summary>
        /// Deletes a film.
        /// </summary>
        /// <param name="id">Id of film.</param>
        /// <returns>A boolean.</returns>
        public bool DeleteFilm(int id)
        {
            Film entity = this.GetFilmByID(id);
            if (entity == null)
            {
                return false;
            }

            this.db.Films.Remove(this.GetFilmByID(id));
            this.db.SaveChanges();
            return true;
        }

        // HELPER

        /// <summary>
        /// Returns the last item's ID.
        /// </summary>
        /// <returns>The last item's ID.</returns>
        public int LastID()
        {
            return (int)this.GetAll().Max(t => t.filmID);
        }

        /// <summary>
        /// Changes a film object's data.
        /// </summary>
        /// <param name="filmId">Id of film.</param>
        /// <param name="cim">Title of film.</param>
        /// <param name="mufaj">Genre of film.</param>
        /// <param name="hossz">Length of a film.</param>
        /// <param name="ev">Year of a film.</param>
        /// <param name="foszereplo">Main actor of film.</param>
        /// <returns>A boolean.</returns>
        public bool ChangeFilm(int filmId, string cim, string mufaj, int hossz, int ev, string foszereplo)
        {
            Film entity = this.GetFilmByID(filmId);
            if (entity == null)
            {
                return false;
            }

            entity.cim = cim;
            entity.mufaj = mufaj;
            entity.hossz = hossz;
            entity.ev = ev;
            entity.foszereplo = foszereplo;
            this.db.SaveChanges();
            return true;
        }
    }
}
