﻿// <copyright file="MozimusorRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCinema.Data;

    /// <summary>
    /// Mozimusor repository class.
    /// </summary>
    public class MozimusorRepository : IMozimusorRepository
    {
        private MyCinemaDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="MozimusorRepository"/> class.
        /// Constructor.
        /// </summary>
        public MozimusorRepository()
        {
            this.db = MyCinemaDatabase.CinemaDB;
        }

        /// <summary>
        /// Gets a T generic type collection.
        /// </summary>
        /// <returns>A list.</returns>
        public IQueryable<Mozimusor> GetAll()
        {
            return this.db.Mozimusors;
        }

        // CREATE

        /// <summary>
        /// Adds a new programme to the table.
        /// </summary>
        /// <param name="ujmusor">The new cinema programme object.</param>
        public void AddMozimusor(Mozimusor ujmusor)
        {
            this.db.Mozimusors.Add(ujmusor);
            this.db.SaveChanges();
        }

        // READ

        /// <summary>
        /// Gets a specific programme.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A programme object.</returns>
        public Mozimusor GetMozimusorByID(int id)
        {
            return this.db.Mozimusors.FirstOrDefault(t => t.mozimusorID == id);
        }

        // UPDATE

        /// <summary>
        /// Updates the income of a programme.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujbevetel">Item's new income.</param>
        public void UpdateBevetel(int id, int ujbevetel)
        {
            var musor = this.GetMozimusorByID(id);
            musor.bevetel = ujbevetel;
            this.db.SaveChanges();
        }

        // UPDATE

        /// <summary>
        /// Updates the number of audience of a programme.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujnezoszam">Item's new number of audience.</param>
        public void UpdateNezoszam(int id, int ujnezoszam)
        {
            var musor = this.GetMozimusorByID(id);
            musor.nezoszam = ujnezoszam;
            this.db.SaveChanges();
        }

        // DELETE

        /// <summary>
        /// Deletes a specific programme.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        public void DeleteMusor(int id)
        {
            this.db.Mozimusors.Remove(this.GetMozimusorByID(id));
            this.db.SaveChanges();
        }

        // HELPER

        /// <summary>
        /// Returns the last item's ID.
        /// </summary>
        /// <returns>The last item's ID.</returns>
        public int LastID()
        {
            return (int)this.GetAll().Max(t => t.mozimusorID);
        }
    }
}
