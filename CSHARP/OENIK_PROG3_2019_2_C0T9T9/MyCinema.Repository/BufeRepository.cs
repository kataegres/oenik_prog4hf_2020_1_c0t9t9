﻿// <copyright file="BufeRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCinema.Data;

    /// <summary>
    /// Bufe repository class.
    /// </summary>
    public class BufeRepository : IBufeRepository
    {
        private readonly MyCinemaDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="BufeRepository"/> class.
        /// Constructor.
        /// </summary>
        public BufeRepository()
        {
            this.db = MyCinemaDatabase.CinemaDB;
        }

        /// <summary>
        /// Gets a T generic type collection.
        /// </summary>
        /// <returns>A list.</returns>
        public IQueryable<Bufe> GetAll()
        {
            return this.db.Bufes;
        }

        // CREATE

        /// <summary>
        /// Adds a new food item to the table.
        /// </summary>
        /// <param name="newfood">A food object.</param>
        public void AddFood(Bufe newfood)
        {
            this.db.Bufes.Add(newfood);
            this.db.SaveChanges();
        }

        // READ

        /// <summary>
        /// Gets a specific food.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A food object.</returns>
        public Bufe GetFoodByID(int id)
        {
            return this.db.Bufes.FirstOrDefault(t => t.termekID == id);
        }

        // UPDATE

        /// <summary>
        /// Updates the price of a food.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujAr">Item's new price.</param>
        public void UpdateAr(int id, int ujAr)
        {
            var termek = this.GetFoodByID(id);
            termek.ar = ujAr;
            this.db.SaveChanges();
        }

        // UPDATE

        /// <summary>
        /// Updates the number of allergens of a food.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="uj">Item's new number of allergens.</param>
        public void UpdateAllergen(int id, int uj)
        {
            var termek = this.GetFoodByID(id);
            termek.allergen = uj;
            this.db.SaveChanges();
        }

        // UPDATE

        /// <summary>
        /// Updates the temperature of a food (hot/cold).
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="uj">Item's new temperature.</param>
        public void UpdateHomerseklet(int id, string uj)
        {
            var termek = this.GetFoodByID(id);
            termek.homerseklet = uj;
            this.db.SaveChanges();
        }

        // DELETE

        /// <summary>
        /// Deletes a specific food object.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        public void DeleteFood(int id)
        {
            this.db.Bufes.Remove(this.GetFoodByID(id));
            this.db.SaveChanges();
        }

        // HELPER

        /// <summary>
        /// Returns the last item's ID.
        /// </summary>
        /// <returns>The last item's ID.</returns>
        public int LastID()
        {
            return (int)this.GetAll().Max(t => t.termekID);
        }
    }
}
