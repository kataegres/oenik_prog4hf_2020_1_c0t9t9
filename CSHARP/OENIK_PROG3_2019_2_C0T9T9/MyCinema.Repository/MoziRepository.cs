﻿// <copyright file="MoziRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCinema.Data;

    /// <summary>
    /// Mozi repository class.
    /// </summary>
    public class MoziRepository : IMoziRepository
    {
        private MyCinemaDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="MoziRepository"/> class.
        /// Constructor.
        /// </summary>
        public MoziRepository()
        {
            this.db = MyCinemaDatabase.CinemaDB;
        }

        /// <summary>
        /// Gets a T generic type collection.
        /// </summary>
        /// <returns>A list.</returns>
        public IQueryable<Mozi> GetAll()
        {
            return this.db.Mozis;
        }

        // CREATE

        /// <summary>
        /// Adds a new cinema to the table.
        /// </summary>
        /// <param name="newmozi">A cinema object.</param>
        public void AddMozi(Mozi newmozi)
        {
            this.db.Mozis.Add(newmozi);
            this.db.SaveChanges();
        }

        // READ

        /// <summary>
        /// Gets a specific cinema.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A cinema object.</returns>
        public Mozi GetMoziByID(int id)
        {
            return this.db.Mozis.FirstOrDefault(t => t.moziID == id);
        }

        // UPDATE

        /// <summary>
        /// Updates if the cinema plays 3D movies (yes/no).
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="uj">Yes/no.</param>
        public void Update3D(int id, string uj)
        {
            var mozi = this.GetMoziByID(id);
            mozi.vane3D = uj;
            this.db.SaveChanges();
        }

        // UPDATE

        /// <summary>
        /// Updates if the cinema plays subtitled movies (yes/no).
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="uj">Yes/no.</param>
        public void UpdateFelirat(int id, string uj)
        {
            var mozi = this.GetMoziByID(id);
            mozi.vanefelirat = uj;
            this.db.SaveChanges();
        }

        // UPDATE

        /// <summary>
        /// Updates the number of vacant seats of a cinema.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujferohely">Item's new number of seats.</param>
        public void UpdateFerohely(int id, int ujferohely)
        {
            var mozi = this.GetMoziByID(id);
            mozi.ferohely = ujferohely;
            this.db.SaveChanges();
        }

        // DELETE

        /// <summary>
        /// Deletes a specific cinema.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        public void DeleteMozi(int id)
        {
            this.db.Mozis.Remove(this.GetMoziByID(id));
            this.db.SaveChanges();
        }

        // HELPER

        /// <summary>
        /// Returns the last item's ID.
        /// </summary>
        /// <returns>The last item's ID.</returns>
        public int LastID()
        {
            return (int)this.GetAll().Max(t => t.moziID);
        }
    }
}
