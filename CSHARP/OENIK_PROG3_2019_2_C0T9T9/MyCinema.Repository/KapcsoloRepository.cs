﻿// <copyright file="KapcsoloRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCinema.Data;

    /// <summary>
    /// .
    /// </summary>
    public class KapcsoloRepository : IKapcsoloRepository
    {
        private MyCinemaDatabaseEntities db;

        /// <summary>
        /// Initializes a new instance of the <see cref="KapcsoloRepository"/> class.
        /// Constructor.
        /// </summary>
        public KapcsoloRepository()
        {
            this.db = MyCinemaDatabase.CinemaDB;
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        public void DeleteConnectionByFilmID(int id)
        {
            this.db.Kapcsoloes.Remove(this.GetConnectionByFilmID(id));
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        public void DeleteConnectionByMoziID(int id)
        {
            this.db.Kapcsoloes.Remove(this.GetConnectionByMoziID(id));
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        public void DeleteConnectionByMozimusorID(int id)
        {
            this.db.Kapcsoloes.Remove(this.GetConnectionByMozimusorID(id));
        }

        /// <summary>
        /// .
        /// </summary>
        /// <returns>A list.</returns>
        public IQueryable<Kapcsolo> GetAll()
        {
            return this.db.Kapcsoloes;
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A connection item.</returns>
        public Kapcsolo GetConnectionByFilmID(int id)
        {
            return this.db.Kapcsoloes.FirstOrDefault(t => t.filmID == id);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A connection item.</returns>
        public Kapcsolo GetConnectionByMoziID(int id)
        {
            return this.db.Kapcsoloes.FirstOrDefault(t => t.mozimusorID == id);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A connection item.</returns>
        public Kapcsolo GetConnectionByMozimusorID(int id)
        {
            return this.db.Kapcsoloes.FirstOrDefault(t => t.moziID == id);
        }
    }
}
