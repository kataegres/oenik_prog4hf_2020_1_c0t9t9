﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyCinema.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyCinema.Data;

    /// <summary>
    /// IRepository interface.
    /// </summary>
    /// <typeparam name="T">T generic type.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Gets a T generic type collection.
        /// </summary>
        /// <returns>A list.</returns>
        IQueryable<T> GetAll();
    }

    // BÜFÉ INTERFACE

    /// <summary>
    /// IBufe repository interface.
    /// </summary>
    public interface IBufeRepository : IRepository<Bufe>
    {
        /// <summary>
        /// Adds a new item to the table.
        /// </summary>
        /// <param name="newfood">A food object.</param>
        void AddFood(Bufe newfood);

        /// <summary>
        /// Gets a specific food.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A food object.</returns>
        Bufe GetFoodByID(int id);

        /// <summary>
        /// Updates price of a food.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujAr">Item's new price.</param>
        void UpdateAr(int id, int ujAr);

        /// <summary>
        /// Updates the number of allergens of a food.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="uj">Item's new number of allergens.</param>
        void UpdateAllergen(int id, int uj);

        /// <summary>
        /// Updates temperature (hot/cold).
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="uj">Item's new temperature.</param>
        void UpdateHomerseklet(int id, string uj);

        /// <summary>
        /// Deletes a specific food.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        void DeleteFood(int id);

        /// <summary>
        /// Returns the last item's ID.
        /// </summary>
        /// <returns>The last item's ID.</returns>
        int LastID();
    }

    // FILM INTERFACE

    /// <summary>
    /// IFilm repository interface.
    /// </summary>
    public interface IFilmRepository : IRepository<Film>
    {
        /// <summary>
        /// Adds a new film to the table.
        /// </summary>
        /// <param name="newfilm">The new film object.</param>
        void AddFilm(Film newfilm);

        /// <summary>
        /// Gets a specific film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A film object.</returns>
        Film GetFilmByID(int id);

        /// <summary>
        /// Updates the length of a film (min).
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujhossz">Item's new length.</param>
        void UpdateHossz(int id, int ujhossz);

        /// <summary>
        /// Updates the release year of a film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujev">Item's new release year.</param>
        void UpdateEv(int id, int ujev);

        /// <summary>
        /// Updates the main actor/actress of a film.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujfoszereplo">Item's new actor/actress.</param>
        void UpdateFoszereplo(int id, string ujfoszereplo);

        /// <summary>
        /// Changes a film's data.
        /// </summary>
        /// <param name="filmId">Id of film.</param>
        /// <param name="cim">Title of film.</param>
        /// <param name="mufaj">Genre of film.</param>
        /// <param name="hossz">Length of film.</param>
        /// <param name="ev">Year of film.</param>
        /// <param name="foszereplo">Main actir of film.</param>
        /// <returns>A boolean.</returns>
        bool ChangeFilm(int filmId, string cim, string mufaj, int hossz, int ev, string foszereplo);

        /// <summary>
        /// Deletes a specific film object.
        /// </summary>
        /// <param name="id">Id of a film.</param>
        /// <returns>A boolean.</returns>
        bool DeleteFilm(int id);

        /// <summary>
        /// Returns the last item's ID.
        /// </summary>
        /// <returns>The last item's ID.</returns>
        int LastID();
    }

    // MOZI INTERFACE

    /// <summary>
    /// IMozi repository interface.
    /// </summary>
    public interface IMoziRepository : IRepository<Mozi>
    {
        /// <summary>
        /// Adds a new cinema to the table.
        /// </summary>
        /// <param name="newmozi">The new cinema object.</param>
        void AddMozi(Mozi newmozi);

        /// <summary>
        /// Gets a specific cinema.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A cinema object.</returns>
        Mozi GetMoziByID(int id);

        /// <summary>
        /// Updates the number of vacant seats of a cinema.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujferohely">Item's new number of seats.</param>
        void UpdateFerohely(int id, int ujferohely);

        /// <summary>
        /// Updates if the cinema plays 3D films (yes/no).
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="uj">Yes/no.</param>
        void Update3D(int id, string uj);

        /// <summary>
        /// Updates if the cinema plays subtitled films (yes/no).
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="uj">Yes/no.</param>
        void UpdateFelirat(int id, string uj);

        /// <summary>
        /// Deletes a specific cinema.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        void DeleteMozi(int id);

        /// <summary>
        /// Returns the last item's ID.
        /// </summary>
        /// <returns>The last item's ID.</returns>
        int LastID();
    }

    // MOZIMŰSOR INTERFACE

    /// <summary>
    /// IMozimusor repository interface.
    /// </summary>
    public interface IMozimusorRepository : IRepository<Mozimusor>
    {
        /// <summary>
        /// Adds a new programme to the table (date, film title etc).
        /// </summary>
        /// <param name="ujmusor">The new cinema programme object.</param>
        void AddMozimusor(Mozimusor ujmusor);

        /// <summary>
        /// Gets a specific programme.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A programme object.</returns>
        Mozimusor GetMozimusorByID(int id);

        /// <summary>
        /// Updates the number of the audience of a programme.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujnezoszam">Item's new number of audience.</param>
        void UpdateNezoszam(int id, int ujnezoszam);

        /// <summary>
        /// Updates the income of a programme (HUF).
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <param name="ujbevetel">Item's new income.</param>
        void UpdateBevetel(int id, int ujbevetel);

        /// <summary>
        /// Deletes a specific programme from the repertoire.
        /// </summary>
        /// <param name="id">Item's ID.</param>
        void DeleteMusor(int id);

        /// <summary>
        /// Returns the last item's ID.
        /// </summary>
        /// <returns>The last item's ID.</returns>
        int LastID();
    }

    // KAPCSOLÓ INTERFACE

    /// <summary>
    ///  IKapcsolo repository interface.
    /// </summary>
    public interface IKapcsoloRepository : IRepository<Kapcsolo>
    {
        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        void DeleteConnectionByFilmID(int id);

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        void DeleteConnectionByMozimusorID(int id);

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        void DeleteConnectionByMoziID(int id);

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A connector item.</returns>
        Kapcsolo GetConnectionByFilmID(int id);

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A connector item.</returns>
        Kapcsolo GetConnectionByMozimusorID(int id);

        /// <summary>
        /// .
        /// </summary>
        /// <param name="id">Item's ID.</param>
        /// <returns>A connector item.</returns>
        Kapcsolo GetConnectionByMoziID(int id);
    }
}
